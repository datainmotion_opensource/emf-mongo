## EMF Mongo

This project provides components to use a MongoDB as back-end for EMF objects.

There are two different approaches.

* Using MongoDB codecs to directly read or write into EObjects
* Using the EMF Resource framework to save and load data

Test are currently disabled in the CI build, because they need a running MongoDB. 
To run the test locally you need a running MongoDB.

Until now the project has no other dependencies than to EMF and the MongoDB driver.
There is a first example in the project:

`tests/org.eclipselabs.emf.mongo.tests`

Just run

`org.eclipselabs.emf.mongo.tests.MongoCodeTest#main()`

The second approach was inspired by Bryan Hunts mongo-emf:
https://github.com/BryanHunt/mongo-emf

Currently only Mongo driver version > 3.2 is supported in the code 