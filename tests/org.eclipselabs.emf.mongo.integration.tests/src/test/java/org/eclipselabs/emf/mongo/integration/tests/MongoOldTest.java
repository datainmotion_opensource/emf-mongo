package org.eclipselabs.emf.mongo.integration.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.bson.Document;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipselabs.emf.mongo.DocumentBuilderFactory;
import org.eclipselabs.emf.mongo.EObjectBuilderFactory;
import org.eclipselabs.emf.mongo.InputStreamFactory;
import org.eclipselabs.emf.mongo.Options;
import org.eclipselabs.emf.mongo.OutputStreamFactory;
import org.eclipselabs.emf.mongo.QueryEngine;
import org.eclipselabs.emf.mongo.converter.ConverterService;
import org.eclipselabs.emf.mongo.tests.person.Contact;
import org.eclipselabs.emf.mongo.tests.person.ContactContextType;
import org.eclipselabs.emf.mongo.tests.person.ContactType;
import org.eclipselabs.emf.mongo.tests.person.GenderType;
import org.eclipselabs.emf.mongo.tests.person.Person;
import org.eclipselabs.emf.mongo.tests.person.PersonFactory;
import org.eclipselabs.emf.osgi.ResourceSetConfigurator;
import org.eclipselabs.emf.osgi.ResourceSetFactory;
import org.eclipselabs.emf.osgi.UriHandlerProvider;
import org.eclipselabs.emfosgi.ECollection;
import org.eclipselabs.mongo.osgi.api.MongoClientProvider;
import org.eclipselabs.mongo.osgi.api.MongoDatabaseProvider;
import org.eclipselabs.mongo.osgi.api.MongoIdFactory;
import org.eclipselabs.mongo.osgi.configuration.ConfigurationProperties;
import org.eclipselabs.mongo.osgi.integration.tests.MongoProviderCustomizer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.util.tracker.ServiceTracker;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.client.MongoCollection;

public class MongoOldTest {

	private ResourceSet resourceSet;
	private MongoClient client;
	private MongoCollection<?> collection;
	private String mongoHost;

	@Before
	public void setup() throws BundleException {
		MongoClientOptions options = MongoClientOptions.builder().build();
		mongoHost = System.getProperty("mongo.host", "localhost");
		client = new MongoClient(mongoHost, options);
		Bundle bundle = Platform.getBundle("org.eclipse.equinox.cm");
		if(bundle == null){
			throw new MissingResourceException("The bundle org.eclipse.equinox.cm is missing!", null, null);
		}
		if(bundle != null && bundle.getState() == Bundle.RESOLVED){
			bundle.start();
		}
	}

	@After
	public void tearDown() throws InterruptedException {
		if (collection != null) {
			collection.drop();
		}
		if (client != null) {
			client.close();
		}
		resourceSet = null;
	}

	@Test
	public void testEMFMongo() throws BundleException, InvalidSyntaxException, IOException, InterruptedException {
		// get some of the bundles to test, to get the bundle context
		Bundle bundle = FrameworkUtil.getBundle(MongoDatabaseProvider.class);
		assertEquals("org.eclipselabs.mongo.osgi.api", bundle.getSymbolicName());
		bundle.start();
		assertEquals(Bundle.ACTIVE, bundle.getState());

		// get bundle context
		BundleContext context = bundle.getBundleContext();
		assertNotNull(context);

		// service lookup for configuration admin service
		ServiceReference<?>[] allServiceReferences = context.getAllServiceReferences(ConfigurationAdmin.class.getName(), null);
		assertNotNull(allServiceReferences);
		assertEquals(1, allServiceReferences.length);
		ServiceReference<?> cmRef = allServiceReferences[0];
		Object service = context.getService(cmRef);
		assertNotNull(service);
		assertTrue(service instanceof ConfigurationAdmin);

		// create mongo client configuration
		ConfigurationAdmin cm = (ConfigurationAdmin) service;
		Configuration clientConfig = cm.getConfiguration(ConfigurationProperties.CLIENT_PID);
		assertNotNull(clientConfig);

		// has to be a new configuration
		Dictionary<String, Object> p = clientConfig.getProperties();
		assertNull(p);

		// add service properties
		String clientId = "testClient";
		String clientUri = "mongodb://" + mongoHost + ":27017";
		p = new Hashtable<String, Object>();
		p.put(MongoClientProvider.PROP_CLIENT_ID, clientId);
		p.put(MongoClientProvider.PROP_URI, clientUri);
		clientConfig.update(p);

		// now track the MongoClientProvider service
		final CountDownLatch createLatch = new CountDownLatch(1);
		MongoProviderCustomizer<MongoClientProvider, MongoClientProvider> customizer = new MongoProviderCustomizer<MongoClientProvider, MongoClientProvider>(context, createLatch);
		ServiceTracker<MongoClientProvider, MongoClientProvider> tracker = new ServiceTracker<MongoClientProvider, MongoClientProvider>(context, MongoClientProvider.class, customizer);
		tracker.open(true);

		// create data base provider configuration
		Configuration databaseConfig = cm.getConfiguration(ConfigurationProperties.DATABASE_PID);
		assertNotNull(databaseConfig);

		// add service properties
		String dbAlias = "testDB";
		String db = "test";
		Dictionary<String, Object> dbp = new Hashtable<String, Object>();
		dbp.put(MongoDatabaseProvider.PROP_ALIAS, dbAlias);
		dbp.put(MongoDatabaseProvider.PROP_DATABASE, db);
		databaseConfig.update(dbp);
		// now track the MongoDatabaseProvider service
		CountDownLatch dbCreateLatch = new CountDownLatch(1);
		MongoProviderCustomizer<MongoDatabaseProvider, MongoDatabaseProvider> dbCustomizer = new MongoProviderCustomizer<MongoDatabaseProvider, MongoDatabaseProvider>(context, dbCreateLatch);
		ServiceTracker<MongoDatabaseProvider, MongoDatabaseProvider> dbTracker = new ServiceTracker<MongoDatabaseProvider, MongoDatabaseProvider>(context, MongoDatabaseProvider.class, dbCustomizer);
		dbTracker.open(true);

		EObjectBuilderFactory eoBF = getService(EObjectBuilderFactory.class);
		assertNotNull(eoBF);
		DocumentBuilderFactory dBF = getService(DocumentBuilderFactory.class);
		assertNotNull(dBF);
		MongoIdFactory idF = getService(MongoIdFactory.class);
		assertNotNull(idF);
		QueryEngine qe = getService(QueryEngine.class);
		assertNotNull(qe);
		ConverterService cs = getService(ConverterService.class);
		assertNotNull(cs);
		InputStreamFactory isf = getService(InputStreamFactory.class);
		assertNotNull(isf);
		OutputStreamFactory osf = getService(OutputStreamFactory.class);
		assertNotNull(osf);
		UriHandlerProvider uhp = getService(UriHandlerProvider.class);
		assertNotNull(uhp);
		ResourceSetConfigurator rsc = getService(ResourceSetConfigurator.class);
		assertNotNull(rsc);
		ResourceSetFactory rsf = getService(ResourceSetFactory.class);
		assertNotNull(rsf);

		databaseConfig.delete();
	}

	/**
	 * Test creation of objects
	 * @throws IOException 
	 */
	@Test
	public void testCreateObjects_Containment() {

//				ResourceSetFactory rsf = provider.createResourceSetFactory();
//				assertNotNull(rsf);
//				resourceSet = rsf.createResourceSet();
//				assertNotNull(resourceSet);
//				assertTrue(resourceSet.getResourceFactoryRegistry().getProtocolToFactoryMap().containsKey("mongodb"));
//				// get collections and clear it
//				MongoCollection<Document> personCollection = client.getDatabase("test").getCollection("Person");
//				personCollection.drop();
//				
//				// create contacts
//				Contact c1 = PersonFactory.eINSTANCE.createContact();
//				c1.setContext(ContactContextType.PRIVATE);
//				c1.setType(ContactType.SKYPE);
//				c1.setValue("charles-brown");
//				Contact c2 = PersonFactory.eINSTANCE.createContact();
//				c2.setContext(ContactContextType.WORK);
//				c2.setType(ContactType.EMAIL);
//				c2.setValue("mark.hoffmann@swarco.de");
//				
//				// create person
//				Person p1 = PersonFactory.eINSTANCE.createPerson();
//				p1.setFirstName("Mark");
//				p1.setLastName("Hoffmann");
//				p1.setGender(GenderType.MALE);
//				// add contacts as containment
//				p1.getContact().add(c1);
//				p1.getContact().add(c2);
//				// insert person
//				assertEquals(0, personCollection.count());
//				
//				Resource resource = resourceSet.createResource(URI.createURI("mongodb://localhost:27017/test/Person/1"));
//				resource.getContents().add(p1);
//				try {
//					resource.save(null);
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//				assertEquals(1, personCollection.count());
//				
//				personCollection.drop();
	}

	/**
	 * Test creation of objects and returning results
	 * @throws IOException 
	 * @throws BundleException 
	 * @throws InvalidSyntaxException 
	 * @throws InterruptedException 
	 */
	@Test
	public void testCreateAndFindObjects_ContainmentMany() throws IOException, BundleException, InvalidSyntaxException, InterruptedException {
		// get some of the bundles to test, to get the bundle context
		Bundle bundle = FrameworkUtil.getBundle(MongoDatabaseProvider.class);
		assertEquals("org.eclipselabs.mongo.osgi.api", bundle.getSymbolicName());
		bundle.start();
		assertEquals(Bundle.ACTIVE, bundle.getState());

		// get bundle context
		BundleContext context = bundle.getBundleContext();
		assertNotNull(context);

		// service lookup for configuration admin service
		ServiceReference<?>[] allServiceReferences = context.getAllServiceReferences(ConfigurationAdmin.class.getName(), null);
		assertNotNull(allServiceReferences);
		assertEquals(1, allServiceReferences.length);
		ServiceReference<?> cmRef = allServiceReferences[0];
		Object service = context.getService(cmRef);
		assertNotNull(service);
		assertTrue(service instanceof ConfigurationAdmin);

		// create mongo client configuration
		ConfigurationAdmin cm = (ConfigurationAdmin) service;
		Configuration clientConfig = cm.getConfiguration(ConfigurationProperties.CLIENT_PID);
		assertNotNull(clientConfig);

		// has to be a new configuration
		Dictionary<String, Object> props = clientConfig.getProperties();

		// add service properties
		String clientId = "testClient";
		String clientUri = "mongodb://" + mongoHost + ":27017";
		System.out.println("Client URI: " + clientUri);
		props = new Hashtable<String, Object>();
		props.put(MongoClientProvider.PROP_CLIENT_ID, clientId);
		props.put(MongoClientProvider.PROP_URI, clientUri);
		clientConfig.update(props);

		// now track the MongoClientProvider service
		final CountDownLatch createLatch = new CountDownLatch(1);
		MongoProviderCustomizer<MongoClientProvider, MongoClientProvider> customizer = new MongoProviderCustomizer<MongoClientProvider, MongoClientProvider>(context, createLatch);
		ServiceTracker<MongoClientProvider, MongoClientProvider> tracker = new ServiceTracker<MongoClientProvider, MongoClientProvider>(context, MongoClientProvider.class, customizer);
		tracker.open(true);

		// create data base provider configuration
		Configuration databaseConfig = cm.getConfiguration(ConfigurationProperties.DATABASE_PID);
		assertNotNull(databaseConfig);

		// add service properties
		String dbAlias = "testDB";
		String db = "test";
		Dictionary<String, Object> dbp = new Hashtable<String, Object>();
		dbp.put(MongoDatabaseProvider.PROP_ALIAS, dbAlias);
		dbp.put(MongoDatabaseProvider.PROP_DATABASE, db);
		databaseConfig.update(dbp);
		// now track the MongoDatabaseProvider service
		CountDownLatch dbCreateLatch = new CountDownLatch(1);
		CountDownLatch dbRemoveLatch = new CountDownLatch(1);
		MongoProviderCustomizer<MongoDatabaseProvider, MongoDatabaseProvider> dbCustomizer = new MongoProviderCustomizer<MongoDatabaseProvider, MongoDatabaseProvider>(context, dbCreateLatch, dbRemoveLatch);
		ServiceTracker<MongoDatabaseProvider, MongoDatabaseProvider> dbTracker = new ServiceTracker<MongoDatabaseProvider, MongoDatabaseProvider>(context, MongoDatabaseProvider.class, dbCustomizer);
		dbTracker.open(true);
		assertTrue("DB Provider was not created in time", dbCreateLatch.await(5, TimeUnit.SECONDS));

		CountDownLatch wait = new CountDownLatch(1);
		wait.await(3, TimeUnit.SECONDS);
		
		ResourceSetFactory rsf = getService(ResourceSetFactory.class);
		assertNotNull(rsf);

		resourceSet = rsf.createResourceSet();
		assertNotNull(resourceSet);
//		assertTrue(resourceSet.getResourceFactoryRegistry().getProtocolToFactoryMap().containsKey("mongodb"));

		System.out.println("Dropping DB");
		MongoCollection<Document> personCollection = client.getDatabase("test").getCollection("Person");
		personCollection.drop();

		// create contacts
		Contact c1 = PersonFactory.eINSTANCE.createContact();
		c1.setContext(ContactContextType.PRIVATE);
		c1.setType(ContactType.SKYPE);
		c1.setValue("charles-brown");
		Contact c2 = PersonFactory.eINSTANCE.createContact();
		c2.setContext(ContactContextType.WORK);
		c2.setType(ContactType.EMAIL);
		c2.setValue("mark.hoffmann@tests.de");

		assertEquals(0, personCollection.count());
		/*
		 * Inserting many persons and with containment contacts
		 */
		int insertSize = 10000;
		int insertBatchSize = 500;

		long start = System.currentTimeMillis();
		List<Person> personsList = new ArrayList<>(insertBatchSize);

		System.out.println("Batch inserting: ");
		Resource resource = resourceSet.createResource(URI.createURI("mongodb://"+ mongoHost + ":27017/test/Person/"));
		Map<?,?> options = Collections.singletonMap(Options.OPTION_FORCE_INSERT, Boolean.TRUE);
		for (int i = 0; i < insertSize; i++) {
			Person person = PersonFactory.eINSTANCE.createPerson();
			person.setFirstName("Mark" + i);
			person.setLastName("Hoffmann" + i);
			person.setGender(GenderType.MALE);
			person.getContact().add(EcoreUtil.copy(c1));
			person.getContact().add(EcoreUtil.copy(c2));
			personsList.add(person);
			// using insert many
			if (i % (insertBatchSize - 1) == 0 || i == (insertSize - 1)) {
				resource.getContents().addAll(personsList);
				resource.save(options);
				if(personsList.size() > 1){
					assertTrue(resource.getContents().size() == 0);
				} else {
					resource.getContents().clear();
				}
				personsList.clear();
			}
		}
		System.out.println("Insert of " + insertSize  + " persons with batchSize=" + insertBatchSize + " took " + (System.currentTimeMillis() - start) + " ms");
		assertEquals(insertSize, personCollection.count());

		/*
		 * Find person in the collection
		 */
		start = System.currentTimeMillis();
		Resource findResource = resourceSet.createResource(URI.createURI("mongodb://" + mongoHost + ":27017/test/Person/?{}"));
		resourceSet.getLoadOptions().put(Options.OPTION_BATCH_SIZE, Integer.valueOf(insertBatchSize));
		findResource.load(null);
		// get the persons
		System.out.println("Finding all persons with a size " + insertSize  + " took " + (System.currentTimeMillis() - start) + " ms");
		assertNotNull(findResource);
		assertFalse(findResource.getContents().isEmpty());
		assertEquals(1, findResource.getContents().size());

		ECollection resultCollection = (ECollection) findResource.getContents().get(0);

		/*
		 * Iterating over the result and getting the real objects
		 */
		start = System.currentTimeMillis();
		List<Person> resultList = new ArrayList<Person>();
		assertEquals(0, resultList.size());
		// iterate over all elements
		for (EObject object : resultCollection.getValues()) {
			Person person = (Person) object;
			resultList.add(person);
		}
		System.out.println("Iterating over all persons and mapping with a batch size " + insertBatchSize  + " took " + (System.currentTimeMillis() - start) + " ms");
		assertEquals(insertSize, resultList.size());

		// doing some object checks
		Person p = resultList.get(500);
		assertEquals("Mark500", p.getFirstName());
		assertEquals("Hoffmann500", p.getLastName());
		assertEquals(GenderType.MALE, p.getGender());
		assertEquals(2, p.getContact().size());
		assertEquals("charles-brown", p.getContact().get(0).getValue());
		assertEquals("mark.hoffmann@tests.de", p.getContact().get(1).getValue());

		p = resultList.get(2500);
		assertEquals("Mark2500", p.getFirstName());
		assertEquals("Hoffmann2500", p.getLastName());
		assertEquals(GenderType.MALE, p.getGender());
		assertEquals(2, p.getContact().size());
		assertEquals("charles-brown", p.getContact().get(0).getValue());
		assertEquals("mark.hoffmann@tests.de", p.getContact().get(1).getValue());

		p = resultList.get(8999);
		assertEquals("Mark8999", p.getFirstName());
		assertEquals("Hoffmann8999", p.getLastName());
		assertEquals(GenderType.MALE, p.getGender());
		assertEquals(2, p.getContact().size());
		assertEquals("charles-brown", p.getContact().get(0).getValue());
		assertEquals("mark.hoffmann@tests.de", p.getContact().get(1).getValue());

		personCollection.drop();
		databaseConfig.delete();
		assertTrue(dbRemoveLatch.await(2, TimeUnit.SECONDS));
		dbTracker.close();
	}
	/**
	 * Test creation of objects and returning results. It uses partitioning of a certain collection.
	 * Partitioning is handles using the 
	 * @throws IOException 
	 * @throws BundleException 
	 * @throws InvalidSyntaxException 
	 * @throws InterruptedException 
	 */
	@Test
	public void testCreateAndFindObjects_CollectionPartitioning() throws IOException, BundleException, InvalidSyntaxException, InterruptedException {
		// get some of the bundles to test, to get the bundle context
		Bundle bundle = FrameworkUtil.getBundle(MongoDatabaseProvider.class);
		assertEquals("org.eclipselabs.mongo.osgi.api", bundle.getSymbolicName());
		bundle.start();
		assertEquals(Bundle.ACTIVE, bundle.getState());
		
		// get bundle context
		BundleContext context = bundle.getBundleContext();
		assertNotNull(context);
		
		// service lookup for configuration admin service
		ServiceReference<?>[] allServiceReferences = context.getAllServiceReferences(ConfigurationAdmin.class.getName(), null);
		assertNotNull(allServiceReferences);
		assertEquals(1, allServiceReferences.length);
		ServiceReference<?> cmRef = allServiceReferences[0];
		Object service = context.getService(cmRef);
		assertNotNull(service);
		assertTrue(service instanceof ConfigurationAdmin);
		
		// create mongo client configuration
		ConfigurationAdmin cm = (ConfigurationAdmin) service;
		Configuration clientConfig = cm.getConfiguration(ConfigurationProperties.CLIENT_PID);
		assertNotNull(clientConfig);
		
		// has to be a new configuration
		Dictionary<String, Object> props = clientConfig.getProperties();
		
		// add service properties
		String clientId = "testClient";
		String clientUri = "mongodb://" + mongoHost + ":27017";
		System.out.println("Client URI: " + clientUri);
		props = new Hashtable<String, Object>();
		props.put(MongoClientProvider.PROP_CLIENT_ID, clientId);
		props.put(MongoClientProvider.PROP_URI, clientUri);
		clientConfig.update(props);
		
		// now track the MongoClientProvider service
		final CountDownLatch createLatch = new CountDownLatch(1);
		MongoProviderCustomizer<MongoClientProvider, MongoClientProvider> customizer = new MongoProviderCustomizer<MongoClientProvider, MongoClientProvider>(context, createLatch);
		ServiceTracker<MongoClientProvider, MongoClientProvider> tracker = new ServiceTracker<MongoClientProvider, MongoClientProvider>(context, MongoClientProvider.class, customizer);
		tracker.open(true);
		
		// create data base provider configuration
		Configuration databaseConfig = cm.getConfiguration(ConfigurationProperties.DATABASE_PID);
		assertNotNull(databaseConfig);
		
		// add service properties
		String dbAlias = "testDB";
		String db = "test";
		Dictionary<String, Object> dbp = new Hashtable<String, Object>();
		dbp.put(MongoDatabaseProvider.PROP_ALIAS, dbAlias);
		dbp.put(MongoDatabaseProvider.PROP_DATABASE, db);
		databaseConfig.update(dbp);
		// now track the MongoDatabaseProvider service
		CountDownLatch dbCreateLatch = new CountDownLatch(1);
		CountDownLatch dbRemoveLatch = new CountDownLatch(1);
		MongoProviderCustomizer<MongoDatabaseProvider, MongoDatabaseProvider> dbCustomizer = new MongoProviderCustomizer<MongoDatabaseProvider, MongoDatabaseProvider>(context, dbCreateLatch, dbRemoveLatch);
		ServiceTracker<MongoDatabaseProvider, MongoDatabaseProvider> dbTracker = new ServiceTracker<MongoDatabaseProvider, MongoDatabaseProvider>(context, MongoDatabaseProvider.class, dbCustomizer);
		dbTracker.open(true);
		assertTrue("DB Provider was not created in time", dbCreateLatch.await(5, TimeUnit.SECONDS));
		
		CountDownLatch wait = new CountDownLatch(1);
		wait.await(3, TimeUnit.SECONDS);
		
		ResourceSetFactory rsf = getService(ResourceSetFactory.class);
		assertNotNull(rsf);
		
		resourceSet = rsf.createResourceSet();
		assertNotNull(resourceSet);
//		assertTrue(resourceSet.getResourceFactoryRegistry().getProtocolToFactoryMap().containsKey("mongodb"));
		
		System.out.println("Dropping DB");
		MongoCollection<Document> personCollection = client.getDatabase("test").getCollection("Person");
		personCollection.drop();
		String collectionExtension = "test";
		MongoCollection<Document> personExtCollection = client.getDatabase("test").getCollection("Person" + "_" + collectionExtension);
		personExtCollection.drop();
		
		// create contacts
		Contact c1 = PersonFactory.eINSTANCE.createContact();
		c1.setContext(ContactContextType.PRIVATE);
		c1.setType(ContactType.SKYPE);
		c1.setValue("charles-brown");
		Contact c2 = PersonFactory.eINSTANCE.createContact();
		c2.setContext(ContactContextType.WORK);
		c2.setType(ContactType.EMAIL);
		c2.setValue("mark.hoffmann@tests.de");
		
		assertEquals(0, personCollection.count());
		assertEquals(0, personExtCollection.count());
		/*
		 * Inserting many persons and with containment contacts
		 */
		int insertSize = 10000;
		int insertBatchSize = 500;
		
		long start = System.currentTimeMillis();
		List<Person> personsList = new ArrayList<>(insertBatchSize);
		
		System.out.println("Batch inserting: ");
		Resource resource = resourceSet.createResource(URI.createURI("mongodb://"+ mongoHost + ":27017/test/Person/"));
		Map<String,Object> options = new HashMap<>();
		options.put(Options.OPTION_FORCE_INSERT, Boolean.TRUE);
		int extensionCollectionCount = 0;
		boolean useCollectionExtension = false; 
		for (int i = 0; i < insertSize; i++) {
			Person person = PersonFactory.eINSTANCE.createPerson();
			person.setFirstName("Mark" + i);
			person.setLastName("Hoffmann" + i);
			person.setGender(GenderType.MALE);
			person.getContact().add(EcoreUtil.copy(c1));
			person.getContact().add(EcoreUtil.copy(c2));
			personsList.add(person);
			// using insert many
			if (i % (insertBatchSize - 1) == 0 || i == (insertSize - 1)) {
				if (useCollectionExtension) {
					options.put(Options.OPTIONS_COLLECTION_PARTITION_EXTENSION, collectionExtension);
					extensionCollectionCount += personsList.size();
				} else {
					options.remove(Options.OPTIONS_COLLECTION_PARTITION_EXTENSION);
				}
				resource.getContents().addAll(personsList);
				resource.save(options);
				if(personsList.size() > 1){
					assertTrue(resource.getContents().size() == 0);
				} else {
					resource.getContents().clear();
				}
				personsList.clear();
				useCollectionExtension = !useCollectionExtension;
			}
		}
		System.out.println("Insert of " + insertSize  + " persons with batchSize=" + insertBatchSize + " took " + (System.currentTimeMillis() - start) + " ms");
		assertEquals(insertSize - extensionCollectionCount, personCollection.count());
		assertEquals(extensionCollectionCount, personExtCollection.count());
		
		/*
		 * Find person in the collection
		 */
		start = System.currentTimeMillis();
		Resource findResource01 = resourceSet.createResource(URI.createURI("mongodb://" + mongoHost + ":27017/test/Person/?{}"));
		resourceSet.getLoadOptions().put(Options.OPTION_BATCH_SIZE, Integer.valueOf(insertBatchSize));
		findResource01.load(null);
		Resource findResource02 = resourceSet.createResource(URI.createURI("mongodb://" + mongoHost + ":27017/test/Person/?{}"));
		Map<String, Object> loadOptions = new HashMap<>();
		loadOptions.put(Options.OPTION_BATCH_SIZE, Integer.valueOf(insertBatchSize));
		loadOptions.put(Options.OPTIONS_COLLECTION_PARTITION_EXTENSION, collectionExtension);
		findResource02.load(loadOptions);
		// get the persons
		System.out.println("Finding all persons with a size " + insertSize  + " took " + (System.currentTimeMillis() - start) + " ms");
		assertNotNull(findResource01);
		assertFalse(findResource01.getContents().isEmpty());
		assertEquals(1, findResource01.getContents().size());
		assertNotNull(findResource02);
		assertFalse(findResource02.getContents().isEmpty());
		assertEquals(1, findResource02.getContents().size());
		
		ECollection resultCollection = (ECollection) findResource01.getContents().get(0);
		ECollection resultExtCollection = (ECollection) findResource02.getContents().get(0);
		
		/*
		 * Iterating over the result and getting the real objects
		 */
		start = System.currentTimeMillis();
		List<Person> resultList = new ArrayList<Person>();
		assertEquals(0, resultList.size());
		// iterate over all elements
		System.out.println("Result " + resultCollection.getValues().size());
		for (EObject object : resultCollection.getValues()) {
			Person person = (Person) object;
			resultList.add(person);
		}
		System.out.println("Result Ext " + resultExtCollection.getValues().size());
		for (EObject object : resultExtCollection.getValues()) {
			Person person = (Person) object;
			resultList.add(person);
		}
		System.out.println("Iterating over all persons and mapping with a batch size " + insertBatchSize  + " took " + (System.currentTimeMillis() - start) + " ms");
		assertEquals(insertSize, resultList.size());
		
		// doing some object checks
		Person p = resultList.get(500);
		assertEquals("Mark1498", p.getFirstName());
		assertEquals("Hoffmann1498", p.getLastName());
		assertEquals(GenderType.MALE, p.getGender());
		assertEquals(2, p.getContact().size());
		assertEquals("charles-brown", p.getContact().get(0).getValue());
		assertEquals("mark.hoffmann@tests.de", p.getContact().get(1).getValue());
		
		p = resultList.get(2500);
		assertEquals("Mark5494", p.getFirstName());
		assertEquals("Hoffmann5494", p.getLastName());
		assertEquals(GenderType.MALE, p.getGender());
		assertEquals(2, p.getContact().size());
		assertEquals("charles-brown", p.getContact().get(0).getValue());
		assertEquals("mark.hoffmann@tests.de", p.getContact().get(1).getValue());
		
		p = resultList.get(8999);
		assertEquals("Mark8001", p.getFirstName());
		assertEquals("Hoffmann8001", p.getLastName());
		assertEquals(GenderType.MALE, p.getGender());
		assertEquals(2, p.getContact().size());
		assertEquals("charles-brown", p.getContact().get(0).getValue());
		assertEquals("mark.hoffmann@tests.de", p.getContact().get(1).getValue());
		
		personCollection.drop();
		personExtCollection.drop();
		databaseConfig.delete();
		assertTrue(dbRemoveLatch.await(2, TimeUnit.SECONDS));
		dbTracker.close();
	}
	
	/**
	 * Test creation of objects and returning results
	 * @throws IOException 
	 * @throws BundleException 
	 * @throws InvalidSyntaxException 
	 * @throws InterruptedException 
	 */
	@Test
	public void testCreateAndFindAndUpdateAndFindObjects_ContainmentMany() throws IOException, BundleException, InvalidSyntaxException, InterruptedException {
		// get some of the bundles to test, to get the bundle context
		Bundle bundle = FrameworkUtil.getBundle(MongoDatabaseProvider.class);
		assertEquals("org.eclipselabs.mongo.osgi.api", bundle.getSymbolicName());
		bundle.start();
		assertEquals(Bundle.ACTIVE, bundle.getState());
		
		// get bundle context
		BundleContext context = bundle.getBundleContext();
		assertNotNull(context);
		
		// service lookup for configuration admin service
		ServiceReference<?>[] allServiceReferences = context.getAllServiceReferences(ConfigurationAdmin.class.getName(), null);
		assertNotNull(allServiceReferences);
		assertEquals(1, allServiceReferences.length);
		ServiceReference<?> cmRef = allServiceReferences[0];
		Object service = context.getService(cmRef);
		assertNotNull(service);
		assertTrue(service instanceof ConfigurationAdmin);
		
		// create mongo client configuration
		ConfigurationAdmin cm = (ConfigurationAdmin) service;
		Configuration clientConfig = cm.getConfiguration(ConfigurationProperties.CLIENT_PID);
		assertNotNull(clientConfig);
		
		// has to be a new configuration
		Dictionary<String, Object> props = clientConfig.getProperties();
		
		// add service properties
		String clientId = "testClient";
		String clientUri = "mongodb://" + mongoHost + ":27017";
		System.out.println("Client URI: " + clientUri);
		props = new Hashtable<String, Object>();
		props.put(MongoClientProvider.PROP_CLIENT_ID, clientId);
		props.put(MongoClientProvider.PROP_URI, clientUri);
		clientConfig.update(props);
		
		// now track the MongoClientProvider service
		final CountDownLatch createLatch = new CountDownLatch(1);
		MongoProviderCustomizer<MongoClientProvider, MongoClientProvider> customizer = new MongoProviderCustomizer<MongoClientProvider, MongoClientProvider>(context, createLatch);
		ServiceTracker<MongoClientProvider, MongoClientProvider> tracker = new ServiceTracker<MongoClientProvider, MongoClientProvider>(context, MongoClientProvider.class, customizer);
		tracker.open(true);
		
		// create data base provider configuration
		Configuration databaseConfig = cm.getConfiguration(ConfigurationProperties.DATABASE_PID);
		assertNotNull(databaseConfig);
		
		// add service properties
		String dbAlias = "testDB";
		String db = "test";
		Dictionary<String, Object> dbp = new Hashtable<String, Object>();
		dbp.put(MongoDatabaseProvider.PROP_ALIAS, dbAlias);
		dbp.put(MongoDatabaseProvider.PROP_DATABASE, db);
		databaseConfig.update(dbp);
		// now track the MongoDatabaseProvider service
		CountDownLatch dbCreateLatch = new CountDownLatch(1);
		CountDownLatch dbRemoveLatch = new CountDownLatch(1);
		MongoProviderCustomizer<MongoDatabaseProvider, MongoDatabaseProvider> dbCustomizer = new MongoProviderCustomizer<MongoDatabaseProvider, MongoDatabaseProvider>(context, dbCreateLatch, dbRemoveLatch);
		ServiceTracker<MongoDatabaseProvider, MongoDatabaseProvider> dbTracker = new ServiceTracker<MongoDatabaseProvider, MongoDatabaseProvider>(context, MongoDatabaseProvider.class, dbCustomizer);
		dbTracker.open(true);
		assertTrue("DB Provider was not created in time", dbCreateLatch.await(2, TimeUnit.SECONDS));
		
		CountDownLatch wait = new CountDownLatch(1);
		wait.await(3, TimeUnit.SECONDS);
		
		ResourceSetFactory rsf = getService(ResourceSetFactory.class);
		assertNotNull(rsf);
		
		resourceSet = rsf.createResourceSet();
		assertNotNull(resourceSet);
//		assertTrue(resourceSet.getResourceFactoryRegistry().getProtocolToFactoryMap().containsKey("mongodb"));
		
		System.out.println("Dropping DB");
		MongoCollection<Document> personCollection = client.getDatabase("test").getCollection("Person");
		personCollection.drop();
		
		// create contacts
		Contact c1 = PersonFactory.eINSTANCE.createContact();
		c1.setContext(ContactContextType.PRIVATE);
		c1.setType(ContactType.SKYPE);
		c1.setValue("charles-brown");
		Contact c2 = PersonFactory.eINSTANCE.createContact();
		c2.setContext(ContactContextType.WORK);
		c2.setType(ContactType.EMAIL);
		c2.setValue("mark.hoffmann@tests.de");
		
		assertEquals(0, personCollection.count());
		/*
		 * Inserting many persons and with containment contacts
		 */
		int insertSize = 10000;
		int insertBatchSize = 500;
		
		long start = System.currentTimeMillis();
		List<Person> personsList = new ArrayList<>(insertBatchSize);
		
		System.out.println("Batch inserting: ");
		Resource resource = resourceSet.createResource(URI.createURI("mongodb://"+ mongoHost + ":27017/test/Person/"));
		Map<?,?> options = Collections.singletonMap(Options.OPTION_FORCE_INSERT, Boolean.TRUE);
		for (int i = 0; i < insertSize; i++) {
			Person person = PersonFactory.eINSTANCE.createPerson();
			person.setFirstName("Mark" + i);
			person.setLastName("Hoffmann" + i);
			person.setGender(GenderType.MALE);
			person.getContact().add(EcoreUtil.copy(c1));
			person.getContact().add(EcoreUtil.copy(c2));
			personsList.add(person);
			// using insert many
			if (i % (insertBatchSize - 1) == 0 || i == (insertSize - 1)) {
				resource.getContents().addAll(personsList);
				resource.save(options);
				personsList.clear();
				resource.getContents().clear();
			}
		}
		System.out.println("Insert of " + insertSize  + " persons with batchSize=" + insertBatchSize + " took " + (System.currentTimeMillis() - start) + " ms");
		assertEquals(insertSize, personCollection.count());
		
		/*
		 * Find person in the collection
		 */
		start = System.currentTimeMillis();
		Resource findResource = resourceSet.createResource(URI.createURI("mongodb://" + mongoHost + ":27017/test/Person/?{}"));
		resourceSet.getLoadOptions().put(Options.OPTION_BATCH_SIZE, Integer.valueOf(insertBatchSize));
		findResource.load(null);
		// get the persons
		System.out.println("Finding all persons with a size " + insertSize  + " took " + (System.currentTimeMillis() - start) + " ms");
		assertNotNull(findResource);
		assertFalse(findResource.getContents().isEmpty());
		assertEquals(1, findResource.getContents().size());
		
		ECollection resultCollection = (ECollection) findResource.getContents().get(0);
		
		/*
		 * Iterating over the result and getting the real objects
		 */
		start = System.currentTimeMillis();
		List<Person> resultList = new ArrayList<Person>();
		assertEquals(0, resultList.size());
		// iterate over all elements
		for (EObject object : resultCollection.getValues()) {
			Person person = (Person) object;
			resultList.add(person);
		}
		System.out.println("Iterating over all persons and mapping with a batch size " + insertBatchSize  + " took " + (System.currentTimeMillis() - start) + " ms");
		assertEquals(insertSize, resultList.size());
		
		// doing some object checks
		Person p = resultList.get(500);
		assertEquals("Mark500", p.getFirstName());
		assertEquals("Hoffmann500", p.getLastName());
		assertEquals(GenderType.MALE, p.getGender());
		assertEquals(2, p.getContact().size());
		assertEquals("charles-brown", p.getContact().get(0).getValue());
		assertEquals("mark.hoffmann@tests.de", p.getContact().get(1).getValue());
		
		p = resultList.get(2500);
		assertEquals("Mark2500", p.getFirstName());
		assertEquals("Hoffmann2500", p.getLastName());
		assertEquals(GenderType.MALE, p.getGender());
		assertEquals(2, p.getContact().size());
		assertEquals("charles-brown", p.getContact().get(0).getValue());
		assertEquals("mark.hoffmann@tests.de", p.getContact().get(1).getValue());
		
		p = resultList.get(8999);
		assertEquals("Mark8999", p.getFirstName());
		assertEquals("Hoffmann8999", p.getLastName());
		assertEquals(GenderType.MALE, p.getGender());
		assertEquals(2, p.getContact().size());
		assertEquals("charles-brown", p.getContact().get(0).getValue());
		assertEquals("mark.hoffmann@tests.de", p.getContact().get(1).getValue());
		
		resourceSet = rsf.createResourceSet();
		
		start = System.currentTimeMillis();
		
		System.out.println("Batch inserting: ");
		resource = resourceSet.createResource(URI.createURI("mongodb://"+ mongoHost + ":27017/test/Person/"));
		int i = 0;
		personsList.clear();
		for (Person person : resultList) {
			Resource eResource = person.eResource();
			eResource.getContents().clear();
			eResource.unload();
			person.setFirstName(person.getFirstName() + "2");
			personsList.add(person);
			// using insert many
			if (i % (insertBatchSize - 1) == 0 || i == (insertSize - 1)) {
				resource.getContents().addAll(personsList);
				resource.save(null);
				personsList.clear();
				resource.getContents().clear();
			}
			i++;
		}
		System.out.println("Insert of " + insertSize  + " persons with batchSize=" + insertBatchSize + " took " + (System.currentTimeMillis() - start) + " ms");
		assertEquals(insertSize, personCollection.count());
		
		/*
		 * Find person in the collection
		 */
		start = System.currentTimeMillis();
		findResource = resourceSet.createResource(URI.createURI("mongodb://" + mongoHost + ":27017/test/Person/?{}"));
		resourceSet.getLoadOptions().put(Options.OPTION_BATCH_SIZE, Integer.valueOf(insertBatchSize));
		findResource.load(null);
		// get the persons
		System.out.println("Finding all persons with a size " + insertSize  + " took " + (System.currentTimeMillis() - start) + " ms");
		assertNotNull(findResource);
		assertFalse(findResource.getContents().isEmpty());
		assertEquals(1, findResource.getContents().size());
		
		resultCollection = (ECollection) findResource.getContents().get(0);
		
		/*
		 * Iterating over the result and getting the real objects
		 */
		start = System.currentTimeMillis();
		resultList = new ArrayList<Person>();
		assertEquals(0, resultList.size());
		// iterate over all elements
		for (EObject object : resultCollection.getValues()) {
			Person person = (Person) object;
			resultList.add(person);
		}
		System.out.println("Iterating over all persons and mapping with a batch size " + insertBatchSize  + " took " + (System.currentTimeMillis() - start) + " ms");
		assertEquals(insertSize, resultList.size());
		
		// doing some object checks
		p = resultList.get(500);
		assertEquals("Mark5002", p.getFirstName());
		assertEquals("Hoffmann500", p.getLastName());
		assertEquals(GenderType.MALE, p.getGender());
		assertEquals(2, p.getContact().size());
		assertEquals("charles-brown", p.getContact().get(0).getValue());
		assertEquals("mark.hoffmann@tests.de", p.getContact().get(1).getValue());
		
		p = resultList.get(2500);
		assertEquals("Mark25002", p.getFirstName());
		assertEquals("Hoffmann2500", p.getLastName());
		assertEquals(GenderType.MALE, p.getGender());
		assertEquals(2, p.getContact().size());
		assertEquals("charles-brown", p.getContact().get(0).getValue());
		assertEquals("mark.hoffmann@tests.de", p.getContact().get(1).getValue());
		
		p = resultList.get(8999);
		assertEquals("Mark89992", p.getFirstName());
		assertEquals("Hoffmann8999", p.getLastName());
		assertEquals(GenderType.MALE, p.getGender());
		assertEquals(2, p.getContact().size());
		assertEquals("charles-brown", p.getContact().get(0).getValue());
		assertEquals("mark.hoffmann@tests.de", p.getContact().get(1).getValue());
		
		personCollection.drop();
		databaseConfig.delete();
		assertTrue(dbRemoveLatch.await(1, TimeUnit.SECONDS));
		dbTracker.close();
	}

	/**
	 * Test creation of object and returning results
	 * @throws IOException 
	 * @throws BundleException 
	 * @throws InvalidSyntaxException 
	 * @throws InterruptedException 
	 */
	@Test
	public void testCreateContainmentSingle() throws IOException, BundleException, InvalidSyntaxException, InterruptedException {
		// get some of the bundles to test, to get the bundle context
		Bundle bundle = FrameworkUtil.getBundle(MongoDatabaseProvider.class);
		assertEquals("org.eclipselabs.mongo.osgi.api", bundle.getSymbolicName());
		bundle.start();
		assertEquals(Bundle.ACTIVE, bundle.getState());
		
		// get bundle context
		BundleContext context = bundle.getBundleContext();
		assertNotNull(context);
		
		// service lookup for configuration admin service
		ServiceReference<?>[] allServiceReferences = context.getAllServiceReferences(ConfigurationAdmin.class.getName(), null);
		assertNotNull(allServiceReferences);
		assertEquals(1, allServiceReferences.length);
		ServiceReference<?> cmRef = allServiceReferences[0];
		Object service = context.getService(cmRef);
		assertNotNull(service);
		assertTrue(service instanceof ConfigurationAdmin);
		
		// create mongo client configuration
		ConfigurationAdmin cm = (ConfigurationAdmin) service;
		Configuration clientConfig = cm.getConfiguration(ConfigurationProperties.CLIENT_PID);
		assertNotNull(clientConfig);
		
		// has to be a new configuration
		Dictionary<String, Object> props = clientConfig.getProperties();
		
		// add service properties
		String clientId = "testClient";
		String clientUri = "mongodb://" + mongoHost + ":27017";
		System.out.println("Client URI: " + clientUri);
		props = new Hashtable<String, Object>();
		props.put(MongoClientProvider.PROP_CLIENT_ID, clientId);
		props.put(MongoClientProvider.PROP_URI, clientUri);
		clientConfig.update(props);
		
		// now track the MongoClientProvider service
		final CountDownLatch createLatch = new CountDownLatch(1);
		MongoProviderCustomizer<MongoClientProvider, MongoClientProvider> customizer = new MongoProviderCustomizer<MongoClientProvider, MongoClientProvider>(context, createLatch);
		ServiceTracker<MongoClientProvider, MongoClientProvider> tracker = new ServiceTracker<MongoClientProvider, MongoClientProvider>(context, MongoClientProvider.class, customizer);
		tracker.open(true);
		
		// create data base provider configuration
		Configuration databaseConfig = cm.getConfiguration(ConfigurationProperties.DATABASE_PID);
		assertNotNull(databaseConfig);
		
		// add service properties
		String dbAlias = "testDB";
		String db = "test";
		Dictionary<String, Object> dbp = new Hashtable<String, Object>();
		dbp.put(MongoDatabaseProvider.PROP_ALIAS, dbAlias);
		dbp.put(MongoDatabaseProvider.PROP_DATABASE, db);
		databaseConfig.update(dbp);
		// now track the MongoDatabaseProvider service
		CountDownLatch dbCreateLatch = new CountDownLatch(1);
		CountDownLatch dbRemoveLatch = new CountDownLatch(1);
		MongoProviderCustomizer<MongoDatabaseProvider, MongoDatabaseProvider> dbCustomizer = new MongoProviderCustomizer<MongoDatabaseProvider, MongoDatabaseProvider>(context, dbCreateLatch, dbRemoveLatch);
		ServiceTracker<MongoDatabaseProvider, MongoDatabaseProvider> dbTracker = new ServiceTracker<MongoDatabaseProvider, MongoDatabaseProvider>(context, MongoDatabaseProvider.class, dbCustomizer);
		dbTracker.open(true);
		assertTrue("DB Provider was not created in time", dbCreateLatch.await(2, TimeUnit.SECONDS));
		
		CountDownLatch wait = new CountDownLatch(1);
		wait.await(3, TimeUnit.SECONDS);
		
		ResourceSetFactory rsf = getService(ResourceSetFactory.class);
		assertNotNull(rsf);
		
		resourceSet = rsf.createResourceSet();
		assertNotNull(resourceSet);
		
		System.out.println("Dropping DB");
		MongoCollection<Document> personCollection = client.getDatabase("test").getCollection("Person");
		personCollection.drop();
		
		// create contacts
		Contact c1 = PersonFactory.eINSTANCE.createContact();
		c1.setContext(ContactContextType.PRIVATE);
		c1.setType(ContactType.SKYPE);
		c1.setValue("charles-brown");
		Contact c2 = PersonFactory.eINSTANCE.createContact();
		c2.setContext(ContactContextType.WORK);
		c2.setType(ContactType.EMAIL);
		c2.setValue("mark.hoffmann@tests.de");
		
		assertEquals(0, personCollection.count());
		Resource resource = resourceSet.createResource(URI.createURI("mongodb://"+ mongoHost + ":27017/test/Person/"));
		
		Person person = PersonFactory.eINSTANCE.createPerson();
		person.setFirstName("Mark");
		person.setLastName("Hoffmann" );
		person.setGender(GenderType.MALE);
		person.getContact().add(EcoreUtil.copy(c1));
		person.getContact().add(EcoreUtil.copy(c2));
		resource.getContents().add(person);
		resource.save(null);

		resource.getContents().clear();
		resource.unload();
		/*
		 * Find person in the collection
		 */
		long start = System.currentTimeMillis();
		Resource findResource = resourceSet.createResource(URI.createURI("mongodb://" + mongoHost + ":27017/test/Person/" + person.getId()));
		findResource.load(null);
		
		// get the person
		assertNotNull(findResource);
		assertFalse(findResource.getContents().isEmpty());
		assertEquals(1, findResource.getContents().size());
		
	
		// doing some object checks
		Person p = (Person) findResource.getContents().get(0);
		assertEquals("Mark", p.getFirstName());
		assertEquals("Hoffmann", p.getLastName());
		assertEquals(GenderType.MALE, p.getGender());
		assertEquals(2, p.getContact().size());
		assertEquals("charles-brown", p.getContact().get(0).getValue());
		assertEquals("mark.hoffmann@tests.de", p.getContact().get(1).getValue());
		
		personCollection.drop();
		databaseConfig.delete();
		assertTrue(dbRemoveLatch.await(1, TimeUnit.SECONDS));
		dbTracker.close();
	}

	/**
	 * Test creation of object and returning results
	 * @throws IOException 
	 * @throws BundleException 
	 * @throws InvalidSyntaxException 
	 * @throws InterruptedException 
	 */
	@Test
	public void testCreateAndUpdateContainmentSingle() throws IOException, BundleException, InvalidSyntaxException, InterruptedException {
		// get some of the bundles to test, to get the bundle context
		Bundle bundle = FrameworkUtil.getBundle(MongoDatabaseProvider.class);
		assertEquals("org.eclipselabs.mongo.osgi.api", bundle.getSymbolicName());
		bundle.start();
		assertEquals(Bundle.ACTIVE, bundle.getState());
		
		// get bundle context
		BundleContext context = bundle.getBundleContext();
		assertNotNull(context);
		
		// service lookup for configuration admin service
		ServiceReference<?>[] allServiceReferences = context.getAllServiceReferences(ConfigurationAdmin.class.getName(), null);
		assertNotNull(allServiceReferences);
		assertEquals(1, allServiceReferences.length);
		ServiceReference<?> cmRef = allServiceReferences[0];
		Object service = context.getService(cmRef);
		assertNotNull(service);
		assertTrue(service instanceof ConfigurationAdmin);
		
		// create mongo client configuration
		ConfigurationAdmin cm = (ConfigurationAdmin) service;
		Configuration clientConfig = cm.getConfiguration(ConfigurationProperties.CLIENT_PID);
		assertNotNull(clientConfig);
		
		// has to be a new configuration
		Dictionary<String, Object> props = clientConfig.getProperties();
		
		// add service properties
		String clientId = "testClient";
		String clientUri = "mongodb://" + mongoHost + ":27017";
		System.out.println("Client URI: " + clientUri);
		props = new Hashtable<String, Object>();
		props.put(MongoClientProvider.PROP_CLIENT_ID, clientId);
		props.put(MongoClientProvider.PROP_URI, clientUri);
		clientConfig.update(props);
		
		// now track the MongoClientProvider service
		final CountDownLatch createLatch = new CountDownLatch(1);
		MongoProviderCustomizer<MongoClientProvider, MongoClientProvider> customizer = new MongoProviderCustomizer<MongoClientProvider, MongoClientProvider>(context, createLatch);
		ServiceTracker<MongoClientProvider, MongoClientProvider> tracker = new ServiceTracker<MongoClientProvider, MongoClientProvider>(context, MongoClientProvider.class, customizer);
		tracker.open(true);
		
		// create data base provider configuration
		Configuration databaseConfig = cm.getConfiguration(ConfigurationProperties.DATABASE_PID);
		assertNotNull(databaseConfig);
		
		// add service properties
		String dbAlias = "testDB";
		String db = "test";
		Dictionary<String, Object> dbp = new Hashtable<String, Object>();
		dbp.put(MongoDatabaseProvider.PROP_ALIAS, dbAlias);
		dbp.put(MongoDatabaseProvider.PROP_DATABASE, db);
		databaseConfig.update(dbp);
		// now track the MongoDatabaseProvider service
		CountDownLatch dbCreateLatch = new CountDownLatch(1);
		CountDownLatch dbRemoveLatch = new CountDownLatch(1);
		MongoProviderCustomizer<UriHandlerProvider, UriHandlerProvider> dbCustomizer = new MongoProviderCustomizer<UriHandlerProvider, UriHandlerProvider>(context, dbCreateLatch, dbRemoveLatch);
		ServiceTracker<UriHandlerProvider, UriHandlerProvider> dbTracker = new ServiceTracker<UriHandlerProvider, UriHandlerProvider>(context, UriHandlerProvider.class, dbCustomizer);
		dbTracker.open(true);
		assertTrue("DB Provider was not created in time", dbCreateLatch.await(2, TimeUnit.SECONDS));
		
		
		CountDownLatch wait = new CountDownLatch(1);
		wait.await(3, TimeUnit.SECONDS);
		
		ResourceSetFactory rsf = getService(ResourceSetFactory.class);
		assertNotNull(rsf);
		
		resourceSet = rsf.createResourceSet();
		assertNotNull(resourceSet);
		
		System.out.println("Dropping DB");
		MongoCollection<Document> personCollection = client.getDatabase("test").getCollection("Person");
		personCollection.drop();
		
		// create contacts
		Contact c1 = PersonFactory.eINSTANCE.createContact();
		c1.setContext(ContactContextType.PRIVATE);
		c1.setType(ContactType.SKYPE);
		c1.setValue("charles-brown");
		Contact c2 = PersonFactory.eINSTANCE.createContact();
		c2.setContext(ContactContextType.WORK);
		c2.setType(ContactType.EMAIL);
		c2.setValue("mark.hoffmann@tests.de");
		
		assertEquals(0, personCollection.count());
		Resource resource = resourceSet.createResource(URI.createURI("mongodb://"+ mongoHost + ":27017/test/Person/"));
		
		Person person = PersonFactory.eINSTANCE.createPerson();
		person.setFirstName("Mark");
		person.setLastName("Hoffmann" );
		person.setGender(GenderType.MALE);
		person.getContact().add(EcoreUtil.copy(c1));
		person.getContact().add(EcoreUtil.copy(c2));
		resource.getContents().add(person);
		resource.save(null);
		
		resource.getContents().clear();
		resource.unload();
		/*
		 * Find person in the collection
		 */
		long start = System.currentTimeMillis();
		Resource findResource = resourceSet.createResource(URI.createURI("mongodb://" + mongoHost + ":27017/test/Person/" + person.getId()));
		findResource.load(null);
		
		// get the person
		assertNotNull(findResource);
		assertFalse(findResource.getContents().isEmpty());
		assertEquals(1, findResource.getContents().size());
		
		
		// doing some object checks
		Person p = (Person) findResource.getContents().get(0);
		assertEquals("Mark", p.getFirstName());
		assertEquals("Hoffmann", p.getLastName());
		assertEquals(GenderType.MALE, p.getGender());
		assertEquals(2, p.getContact().size());
		assertEquals("charles-brown", p.getContact().get(0).getValue());
		assertEquals("mark.hoffmann@tests.de", p.getContact().get(1).getValue());
		
		findResource.unload();

		person.setFirstName("Mark2");
		
		resource.getContents().add(person);
		resource.save(null);
		
		resource.getContents().clear();
		resource.unload();
		/*
		 * Find person in the collection
		 */
		start = System.currentTimeMillis();
		findResource = resourceSet.createResource(URI.createURI("mongodb://" + mongoHost + ":27017/test/Person/" + person.getId()));
		findResource.load(null);
		
		// get the person
		assertNotNull(findResource);
		assertFalse(findResource.getContents().isEmpty());
		assertEquals(1, findResource.getContents().size());
		
		
		// doing some object checks
		p = (Person) findResource.getContents().get(0);
		assertEquals("Mark2", p.getFirstName());
		assertEquals("Hoffmann", p.getLastName());
		assertEquals(GenderType.MALE, p.getGender());
		assertEquals(2, p.getContact().size());
		assertEquals("charles-brown", p.getContact().get(0).getValue());
		assertEquals("mark.hoffmann@tests.de", p.getContact().get(1).getValue());
		
		personCollection.drop();
		databaseConfig.delete();
		assertTrue(dbRemoveLatch.await(1, TimeUnit.SECONDS));
		dbTracker.close();
	}

	static <T> T getService(Class<T> clazz) {
		Bundle bundle = FrameworkUtil.getBundle(MongoDatabaseProvider.class);
		if (bundle != null) {
			ServiceTracker<T, T> st =
					new ServiceTracker<T, T>(
							bundle.getBundleContext(), clazz, null);
			st.open();
			if (st != null) {
				try {
					// give the runtime some time to startup
					return st.waitForService(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

}
