/**
 */
package org.eclipselabs.emf.mongo.tests.person.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipselabs.emf.mongo.tests.person.BusinessPerson;
import org.eclipselabs.emf.mongo.tests.person.PersonPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Business Person</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BusinessPersonImpl extends PersonImpl implements BusinessPerson {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BusinessPersonImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PersonPackage.Literals.BUSINESS_PERSON;
	}

} //BusinessPersonImpl
