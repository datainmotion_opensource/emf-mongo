/**
 */
package org.eclipselabs.emf.mongo.tests.person.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see org.eclipselabs.emf.mongo.tests.person.util.PersonResourceFactoryImpl
 * @generated
 */
public class PersonResourceImpl extends XMIResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public PersonResourceImpl(URI uri) {
		super(uri);
	}

} //PersonResourceImpl
