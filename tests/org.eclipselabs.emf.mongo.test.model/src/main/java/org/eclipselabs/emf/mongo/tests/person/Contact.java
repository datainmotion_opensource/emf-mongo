/**
 */
package org.eclipselabs.emf.mongo.tests.person;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Contact</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipselabs.emf.mongo.tests.person.Contact#getType <em>Type</em>}</li>
 *   <li>{@link org.eclipselabs.emf.mongo.tests.person.Contact#getContext <em>Context</em>}</li>
 *   <li>{@link org.eclipselabs.emf.mongo.tests.person.Contact#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see org.eclipselabs.emf.mongo.tests.person.PersonPackage#getContact()
 * @model
 * @generated
 */
public interface Contact extends EObject {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipselabs.emf.mongo.tests.person.ContactType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see org.eclipselabs.emf.mongo.tests.person.ContactType
	 * @see #setType(ContactType)
	 * @see org.eclipselabs.emf.mongo.tests.person.PersonPackage#getContact_Type()
	 * @model required="true"
	 * @generated
	 */
	ContactType getType();

	/**
	 * Sets the value of the '{@link org.eclipselabs.emf.mongo.tests.person.Contact#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see org.eclipselabs.emf.mongo.tests.person.ContactType
	 * @see #getType()
	 * @generated
	 */
	void setType(ContactType value);

	/**
	 * Returns the value of the '<em><b>Context</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipselabs.emf.mongo.tests.person.ContactContextType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Context</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Context</em>' attribute.
	 * @see org.eclipselabs.emf.mongo.tests.person.ContactContextType
	 * @see #setContext(ContactContextType)
	 * @see org.eclipselabs.emf.mongo.tests.person.PersonPackage#getContact_Context()
	 * @model required="true"
	 * @generated
	 */
	ContactContextType getContext();

	/**
	 * Sets the value of the '{@link org.eclipselabs.emf.mongo.tests.person.Contact#getContext <em>Context</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Context</em>' attribute.
	 * @see org.eclipselabs.emf.mongo.tests.person.ContactContextType
	 * @see #getContext()
	 * @generated
	 */
	void setContext(ContactContextType value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see org.eclipselabs.emf.mongo.tests.person.PersonPackage#getContact_Value()
	 * @model
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link org.eclipselabs.emf.mongo.tests.person.Contact#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

} // Contact
