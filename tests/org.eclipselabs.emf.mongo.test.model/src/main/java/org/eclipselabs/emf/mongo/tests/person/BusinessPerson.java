/**
 */
package org.eclipselabs.emf.mongo.tests.person;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Business Person</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipselabs.emf.mongo.tests.person.PersonPackage#getBusinessPerson()
 * @model
 * @generated
 */
public interface BusinessPerson extends Person {
} // BusinessPerson
