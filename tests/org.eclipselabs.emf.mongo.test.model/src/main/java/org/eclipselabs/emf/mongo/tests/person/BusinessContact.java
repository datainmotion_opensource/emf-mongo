/**
 */
package org.eclipselabs.emf.mongo.tests.person;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Business Contact</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipselabs.emf.mongo.tests.person.BusinessContact#getCompanyName <em>Company Name</em>}</li>
 * </ul>
 *
 * @see org.eclipselabs.emf.mongo.tests.person.PersonPackage#getBusinessContact()
 * @model
 * @generated
 */
public interface BusinessContact extends Contact {
	/**
	 * Returns the value of the '<em><b>Company Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Company Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Company Name</em>' attribute.
	 * @see #setCompanyName(String)
	 * @see org.eclipselabs.emf.mongo.tests.person.PersonPackage#getBusinessContact_CompanyName()
	 * @model
	 * @generated
	 */
	String getCompanyName();

	/**
	 * Sets the value of the '{@link org.eclipselabs.emf.mongo.tests.person.BusinessContact#getCompanyName <em>Company Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Company Name</em>' attribute.
	 * @see #getCompanyName()
	 * @generated
	 */
	void setCompanyName(String value);

} // BusinessContact
