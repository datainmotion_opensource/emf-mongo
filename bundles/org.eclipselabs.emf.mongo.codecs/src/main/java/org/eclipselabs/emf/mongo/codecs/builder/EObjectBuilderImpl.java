/*******************************************************************************
 * Copyright (c) 2012 Bryan Hunt & Ed Merks.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Bryan Hunt & Ed Merks - initial API and implementation
 *    Juergen Albert & Mark Hoffmann - Refactoring to support mongo stream reader capabilities
 *******************************************************************************/

package org.eclipselabs.emf.mongo.codecs.builder;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.bson.BsonReader;
import org.bson.BsonType;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.configuration.CodecRegistry;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.FeatureMapUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.XMLResource.URIHandler;
import org.eclipse.emf.ecore.xmi.impl.URIHandlerImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipselabs.emf.mongo.Keywords;
import org.eclipselabs.emf.mongo.MongoUtils;
import org.eclipselabs.emf.mongo.converter.ConverterService;

/**
 * This class builds EMF EObjects from a MongoDB DBObject. This builder is designed to be extensible,
 * but you must be aware of the assumptions made by each of the functions in the builder.
 * 
 * This class is thread safe.
 * 
 * @author bhunt
 */
public class EObjectBuilderImpl implements EObjectBuilder {

	private ConverterService converterService;
	private Map<String, EClass> eClassCache;
	private final CodecRegistry codecRegistry;
	private final ResourceSet resourceSet;
	private final Map<?, ?> options;
	private Map<Object, Object> response;
	private URI baseUri;
	private final static XMIResourceFactoryImpl factory = new XMIResourceFactoryImpl();
	private final List<Resource> resourceCache;
	private final URIHandler uriHandler;
	private List<EReference> sourceReferences = new LinkedList<>();
	
	/**
	 * Constructs an object builder without an EClass cache.
	 * 
	 * @param converterService the service to use when converting attribute values
	 * @param uriHandler the handler for creating proxy URIs
	 * @param baseUri the basic {@link URI} to use 
	 * @param includeAttributesForProxyReferences true if you want attribute values to be set on proxy references; false otherwise
	 * @param options the resource load options
	 */
	public EObjectBuilderImpl(ConverterService converterService, URI baseUri, CodecRegistry codecRegistry, ResourceSet resourceSet, Map<?, ?> options, List<Resource> resourceCache) {
		this(converterService, baseUri, new ConcurrentHashMap<>(), codecRegistry, resourceSet, options, resourceCache);
		
	}

	/**
	 * Constructs an object builder with an optional EClass cache.
	 * 
	 * @param converterService the service to use when converting attribute values
	 * @param baseUri the basic {@link URI} to use
	 * @param includeAttributesForProxyReferences true if you want attribute values to be set on proxy references; false otherwise
	 * @param eClassCache the cache to use to EClass lookups when building the EObject instance - may be null
	 * @param options the Resource load options
	 */
	public EObjectBuilderImpl(ConverterService converterService, URI baseUri, Map<String, EClass> eClassCache, CodecRegistry codecRegistry, ResourceSet resourceSet, Map<?, ?> options, List<Resource> resourceCache) {
		this.baseUri = baseUri;
		this.options = options;
		this.converterService = converterService;
		this.eClassCache = eClassCache;
		this.codecRegistry = codecRegistry;
		this.resourceSet = resourceSet;
		this.resourceCache = resourceCache;
		this.response = getResponseOptions(this.options);
		this.uriHandler = getURIHandler(this.options);
	}

	/**
	 * looks for the {@link XMLResource#OPTION_URI_HANDLER} or creates a new instance
	 * @param options the save options {@link Map}
	 * @return the UriHandler desired
	 */
	private URIHandler getURIHandler(Map<?, ?> options2) {
		URIHandler handler = (URIHandler) options.get(XMLResource.OPTION_URI_HANDLER);
		if(handler == null){
			handler = new URIHandlerImpl();
			handler.setBaseURI(baseUri);
		}
		return handler;
	}

	/**
	 * Returns the response options map from the save options map
	 * @param options the save options map
	 * @return the response options map from the save options map
	 */
	@SuppressWarnings("unchecked")
	private Map<Object, Object> getResponseOptions(Map<?, ?> options) {
		return (Map<Object, Object>)options.get(URIConverter.OPTION_RESPONSE);
	}

	/**
	 * Sets the extrensic ID if it exists and the resource is of type XMLResource. The
	 * extrensic ID is expected to be mapped to the key EXTRINSIC_ID_KEY.
	 * 
	 * @param reader the object read from MongoDB
	 * @param resource the resource that will contain the EMF Object
	 * @param eObject the EMF object being built
	 */
	protected void buildExtrinsicID(BsonReader reader, Resource resource, EObject eObject) {
		String id = reader.readString(Keywords.EXTRINSIC_ID_KEY);

		if (id != null && resource != null && resource instanceof XMLResource) {
			((XMLResource) resource).setID(eObject, id);
		}
	}


	/**
	 * Converts the MongoDB value into an EMF value using the converter service
	 * 
	 * @param eDataType the value type
	 * @param dbValue the value
	 * @return the converted value
	 */
	protected Object convertMongoDBValueToEMFValue(EDataType eDataType, Object dbValue)
	{
		Object convertedValue = dbValue;

		if (!MongoUtils.isNativeType(eDataType))
		{
			// Types not native to MongoDB are stored as strings and must be converted to the proper object type by EMF

			convertedValue = converterService.getConverter(eDataType).convertMongoDBValueToEMFValue(eDataType, dbValue);
		}
		else if (dbValue != null)
		{
			// If the type is a byte, float, or short, it must be converted from the native MongoDB type
			// It is valid to use == for string comparison in this case.

			String instanceClassName = eDataType.getInstanceClassName();

			if (instanceClassName == "byte" || instanceClassName == "java.lang.Byte")
				convertedValue = ((Integer) dbValue).byteValue();
			else if (instanceClassName == "float" || instanceClassName == "java.lang.Float")
				convertedValue = ((Double) dbValue).floatValue();
			else if (instanceClassName == "short" || instanceClassName == "java.lang.Short")
				convertedValue = ((Integer) dbValue).shortValue();
		}

		return convertedValue;
	}

	/**
	 * This function creates an empty EObject ether according to the given eClass URI or with reference type 
	 * of the given reference.
	 * 
	 * @param reader the {@link BsonReader} to read from
	 * @param resourceSet the resourceSet that will be used to locate the EClass if it is not cached
	 * @param reference the reference this object is for
	 * @param eClassUri if a eClass attribute has been read before 
	 * @return the newly created object of type as specified by the data read from MongoDB
	 */
	protected EObject createEObject(BsonReader reader, ResourceSet resourceSet, EReference reference, String eClassUri) {
		EClass eClass = eClassUri != null ? getEClass(resourceSet, eClassUri) : reference.getEReferenceType();
		return EcoreUtil.create(eClass);
	}

	/**
	 * Finds the EClass for the given URI
	 * 
	 * @param resourceSet the resource set used to locate the EClass if it was not
	 *          found in the cache
	 * @param eClassURI the URI of the EClass
	 * @return the EClass instance for the given URI
	 */
	protected EClass getEClass(ResourceSet resourceSet, String eClassURI) {
		if (eClassCache != null) {
			synchronized (eClassCache) {
				EClass eClass = eClassCache.get(eClassURI);

				if (eClass == null) {
					eClass = (EClass) resourceSet.getEObject(URI.createURI(eClassURI), true);
					eClassCache.put(eClassURI, eClass);
				}

				return eClass;
			}
		}

		return (EClass) resourceSet.getEObject(URI.createURI(eClassURI), true);
	}

	/* 
	 * (non-Javadoc)
	 * @see org.eclipselabs.emf.mongo.codecs.builder.EObjectBuilder#decodeObject(org.bson.BsonReader, org.bson.codecs.DecoderContext, org.eclipse.emf.ecore.resource.ResourceSet)
	 */
	@Override
	public EObject decodeObject(BsonReader reader, DecoderContext context, Resource resource) {
		
		
		Resource loadResource = resource;
		reader.readBsonType();
		if(!BsonType.END_OF_DOCUMENT.equals(reader.getCurrentBsonType()) && sourceReferences.size() == 0){
			String oid = null;
			String name = reader.readName();
			if(Keywords.ID_KEY.equals(name)){
				oid = getPrimitiveValue(reader, reader.getCurrentBsonType(), null).toString(); 
				reader.readBsonType(); //set the reader to the state requried by the next step.
				URI uri = baseUri.trimSegments(1).appendSegment(oid).trimQuery();
				if(!resource.getURI().equals(uri)){
					loadResource = factory.createResource(uri);
					if(resourceCache != null){
						resourceCache.add(loadResource);
					} else {
						resourceSet.getResources().add(loadResource);
					}
					//TODO we have to set the resource somehow to loaded
//				try {
//					loadResource.load(new ByteArrayInputStream(new byte[0]), null);
//				} catch (IOException e) {
//					// TODO Check if nothing happens here and loaded is set
//					e.printStackTrace();
//				}
				}
			}
		}
		
		String eClassUri = null;
		if(!BsonType.END_OF_DOCUMENT.equals(reader.getCurrentBsonType())){
			String name = reader.readName();
			if(Keywords.ECLASS_KEY.equals(name)){
				eClassUri = reader.readString();
				//set the reader to the next required state to match the expecation of the next step.
				BsonType nextType = reader.readBsonType();
				if(!BsonType.END_OF_DOCUMENT.equals(nextType)){
					reader.readName();
				}
			}
		}
		EObject result = createEObject(reader, resourceSet, getLastReference(sourceReferences), eClassUri);
		if(sourceReferences.size() == 0){
			loadResource.getContents().add(result);
		}
		decodeFeatures(reader, context, result);
		return result;
	}

	/**
	 * Extracts the last added sourceReference or null
	 * @param sourceReference the list of sourceReferences
	 * @return null or the last {@link EReference} used
	 */
	private EReference getLastReference(List<EReference> sourceReference) {
		if(sourceReference.size() == 0){
			return null;
		}
		
		return sourceReference.get(sourceReference.size() -1);
	}

	/* (non-Javadoc)
	 * @see de.dim.spark.test.mongo.builder.EObjectBuilder#decodeFeatures(org.bson.BsonReader, org.bson.codecs.DecoderContext, org.eclipse.emf.ecore.EObject)
	 */
	@Override
	public void decodeFeatures(BsonReader reader, DecoderContext decoderContext, EObject parent) {
		BsonType currentType = reader.getCurrentBsonType();
		EClass eclass = parent.eClass();
		boolean firstCycle = true;
		while (!BsonType.END_OF_DOCUMENT.equals(currentType)) {
			String currentFeatureName = null;
			if(firstCycle){
				currentFeatureName = reader.getCurrentName();
				firstCycle = false;
			} else {
				currentFeatureName = reader.readName();
			}
			if(currentFeatureName.equals(Keywords.TIME_STAMP_KEY)){
				if(response != null){
					response.put(URIConverter.ATTRIBUTE_TIME_STAMP, reader.readInt64());
				} else {
					reader.skipValue();
				}
				currentType = reader.readBsonType();
				continue;
			}
			currentType = reader.getCurrentBsonType();
			EStructuralFeature currentFeature = eclass.getEStructuralFeature(currentFeatureName);

			if (currentFeature != null) {
				if (currentFeature instanceof EAttribute) {
					decodeAttribute(reader, decoderContext, currentType, parent, (EAttribute) currentFeature);
				} else if (currentFeature instanceof EReference) {
					decodeReference(reader, decoderContext, currentType, parent, (EReference)currentFeature);
				}
			} else {
				reader.skipValue();
			}
			currentType = reader.readBsonType();
		}
	}

	/* (non-Javadoc)
	 * @see de.dim.spark.test.mongo.builder.EObjectBuilder#decodeReference(org.bson.BsonReader, org.bson.codecs.DecoderContext, org.bson.BsonType, org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EReference)
	 */
	@Override
	public void decodeReference(BsonReader reader, DecoderContext decoderContext, BsonType type, EObject parent, EReference reference) {
		Object result = null;
		sourceReferences.add(reference);
		try{
			if (!reference.isContainment()) {
				if(reference.isMany() && BsonType.ARRAY.equals(type)){
					List<EObject> children = new LinkedList<>();
					reader.readStartArray();
					while (!BsonType.END_OF_DOCUMENT.equals(reader.readBsonType())) {
						EObject proxy = decodeProxy(reader, reference);
						children.add(proxy);
					} 
					result = children;
					reader.readEndArray();
				} else {
					EObject proxy = decodeProxy(reader, reference);
					result = proxy;
				}
			} else if (reference.isMany() && BsonType.ARRAY.equals(type)) {
				List<EObject> children = new LinkedList<>();
				reader.readStartArray();
				while (!BsonType.END_OF_DOCUMENT.equals(reader.readBsonType())) {
					EObject child = (EObject) codecRegistry.get(reference.getEReferenceType().getInstanceClass()).decode(reader, decoderContext);
					if (child != null) {
						children.add(child);
					}
				} 
				result = children;
				reader.readEndArray();
			} else {
				result = codecRegistry.get(reference.getEReferenceType().getInstanceClass()).decode(reader, decoderContext);
			}
			if (result != null) {
				parent.eSet(reference, result);
			}
		} finally {
			sourceReferences.remove(reference);
		}
	}

	/**
	 * Reads and creates a Proxy
	 * @param reader the {@link BsonReader} to use
	 * @param reference the reference the proxy is for
	 * @return the desired proxy
	 */
	private EObject decodeProxy(BsonReader reader, EReference reference) {
		reader.readStartDocument();
		String proxyString = reader.readString(Keywords.PROXY_KEY);
		BsonType nextType = reader.readBsonType();
		String eClassUri = null;
		if(!BsonType.END_OF_DOCUMENT.equals(nextType)){
			eClassUri = reader.readString();
		}
		EObject proxy = createEObject(reader, resourceSet, reference, eClassUri);
		((InternalEObject) proxy).eSetProxyURI(uriHandler.resolve(URI.createURI(proxyString)));
		reader.readEndDocument();
		return proxy;
	}

	/* (non-Javadoc)
	 * @see de.dim.spark.test.mongo.builder.EObjectBuilder#decodeAttribute(org.bson.BsonReader, org.bson.codecs.DecoderContext, org.bson.BsonType, org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EAttribute)
	 */
	@Override
	public void decodeAttribute(BsonReader reader, DecoderContext decoderContext, BsonType type, EObject parent, EAttribute attribute) {
		if (FeatureMapUtil.isFeatureMap(attribute)) {
			//      buildFeatureMap(collection, resource, eObject, attribute, (List<DBObject>) value);
		} else if (attribute.isMany()) {
			decodeAttributeArray(reader, parent, attribute, type);
		} else {
			decodeAttributeValue(reader, parent, attribute, type);
		}
	}

	/**
	 * Decodes a attribute value
	 * @param reader the {@link BsonReader} to read from
	 * @param parent the parent model object
	 * @param attribute the {@link EAttribute}
	 * @param type the source data type
	 */
	private void decodeAttributeValue(BsonReader reader, EObject parent, EAttribute attribute, BsonType type) {
//		parent.eSet(attribute, convertMongoDBValueToEMFValue(attribute.getEAttributeType(), value));
		Object value = getPrimitiveValue(reader, type, attribute.getEAttributeType());
		parent.eSet(attribute, value);
	}

	/**
	 * Decodes multi value attribute values
	 * @param reader the {@link BsonReader} to read from
	 * @param parent the parent model object to be set
	 * @param attribute the {@link EAttribute}
	 * @param type the source data type
	 */
	private void decodeAttributeArray(BsonReader reader, EObject parent, EAttribute attribute, BsonType type) {
		assert reader != null;
		assert parent != null;
		assert attribute != null;
		assert type != null;     

		List<Object> convertedValues = new LinkedList<Object>();
		EDataType dataType = attribute.getEAttributeType();
		if (BsonType.ARRAY.equals(type)) {
			//      if (!MongoUtils.isNativeType(attribute.getEAttributeType()) && 
			//              BsonType.ARRAY.equals(type)) {
			reader.readStartArray();
			while (!BsonType.END_OF_DOCUMENT.equals(reader.readBsonType())) {
				type = reader.getCurrentBsonType();
				Object object = getPrimitiveValue(reader, type, dataType);
				if (object != null) {
					object = convertMongoDBValueToEMFValue(dataType, object);
					convertedValues.add(object);
				}
			}
			reader.readEndArray();
		} 
		parent.eSet(attribute, convertedValues);
	}

//	private void decodeFeatureMap(BsonReader reader, DecoderContext context, EAttribute attribute) {
//
//	}

	/**
	 * Returns a primitive value from the {@link BsonReader}
	 * @param reader the {@link BsonReader} to read from
	 * @param type the data type
	 * @param eDataType the target EMF data type
	 * @return the extracted {@link Object} from the reader
	 */
	private Object getPrimitiveValue(BsonReader reader, BsonType type, EDataType eDataType) {
		assert reader != null;
		assert type != null;
		switch(type) {
		case OBJECT_ID:
			return reader.readObjectId();
		case BOOLEAN:
			return Boolean.valueOf(reader.readBoolean());
		case INT32:
			return Integer.valueOf(reader.readInt32());
		case INT64:
			return Long.valueOf(reader.readInt64());
		case DOUBLE:
			if(eDataType.getInstanceClass().equals(Float.class) || eDataType.getInstanceClassName().equals("float")){
				return Float.valueOf((float) reader.readDouble());
			} else {
				return Double.valueOf(reader.readDouble());
			}
		case BINARY:
			return reader.readBinaryData().getData();
		case DATE_TIME:
			return new Date(reader.readDateTime());
		case STRING:
			String value = reader.readString();
			if (eDataType instanceof EEnum) {
				EEnumLiteral literal = ((EEnum) eDataType).getEEnumLiteralByLiteral(value);
				return literal.getInstance();
			} else {
				return value;
			}
		case TIMESTAMP:
			return new Date(reader.readTimestamp().asDateTime().getValue());
		case NULL:
		default:
			reader.readNull();
			return null;
		}
	}
}
