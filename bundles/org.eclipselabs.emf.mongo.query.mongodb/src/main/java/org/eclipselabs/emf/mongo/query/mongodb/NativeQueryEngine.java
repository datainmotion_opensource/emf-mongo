/*******************************************************************************
 * Copyright (c) 2013 Bryan Hunt.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Bryan Hunt - initial API and implementation
 *******************************************************************************/

package org.eclipselabs.emf.mongo.query.mongodb;

import org.bson.Document;
import org.eclipse.emf.common.util.URI;
import org.eclipselabs.emf.mongo.QueryEngine;
import org.eclipselabs.emf.mongo.model.EMongoQuery;
import org.eclipselabs.emf.mongo.model.ModelFactory;

/**
 * Implementation of a EMF Mongo query enigne
 * @author Mark Hoffmann
 * @author bhunt
 * @since 03.07.2016
 */
public class NativeQueryEngine implements QueryEngine {

	/* 
	 * (non-Javadoc)
	 * @see org.eclipselabs.emf.mongo.QueryEngine#buildMongoQuery(org.eclipse.emf.common.util.URI)
	 */
	@Override
	public EMongoQuery buildMongoQuery(URI uri) {
		Document query = (Document) Document.parse(URI.decode(uri.query()));

		EMongoQuery mongoQuery = ModelFactory.eINSTANCE.createEMongoQuery();
		mongoQuery.setFilter((Document) query.get("filter"));
		mongoQuery.setProjection((Document) query.get("projection"));
		mongoQuery.setSort((Document) query.get("sort"));
		mongoQuery.setLimit((Integer) query.get("limit"));
		mongoQuery.setSkip((Integer) query.get("skip"));
		return mongoQuery;
	}
}
