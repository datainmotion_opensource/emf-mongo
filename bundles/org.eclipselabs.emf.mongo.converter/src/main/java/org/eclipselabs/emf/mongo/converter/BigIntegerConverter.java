/**
 * 
 */
package org.eclipselabs.emf.mongo.converter;

import java.math.BigInteger;

import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipselabs.emf.mongo.converter.ValueConverter;

/**
 * Converter for BigInteger.
 * @author Sebastian Doerl
 */
public class BigIntegerConverter implements ValueConverter {

	/* 
	 * (non-Javadoc)
	 * @see org.eclipselabs.emf.mongo.converter.ValueConverter#isConverterForType(org.eclipse.emf.ecore.EDataType)
	 */
	@Override
	public boolean isConverterForType(EDataType eDataType) {
		if (eDataType.getInstanceClass().equals(BigInteger.class)) {
			return true;
		}
		return false;
	}
	
	/* 
	 * (non-Javadoc)
	 * @see org.eclipselabs.emf.mongo.converter.ValueConverter#convertMongoDBValueToEMFValue(org.eclipse.emf.ecore.EDataType, java.lang.Object)
	 */
	@Override
	public Object convertMongoDBValueToEMFValue(EDataType eDataType,
			Object databaseValue) {
		if (databaseValue instanceof Integer) {
			return EcoreUtil.createFromString(eDataType, ((Integer)databaseValue).toString());
		}
		if (databaseValue instanceof String) {
			return EcoreUtil.createFromString(eDataType, (String) databaseValue);
		}
		return databaseValue.toString();
	}
	
	/* 
	 * (non-Javadoc)
	 * @see org.eclipselabs.emf.mongo.converter.ValueConverter#convertEMFValueToMongoDBValue(org.eclipse.emf.ecore.EDataType, java.lang.Object)
	 */
	@Override
	public Object convertEMFValueToMongoDBValue(EDataType eDataType,
			Object emfValue) {
		return EcoreUtil.convertToString(eDataType, emfValue);
	}

}
