/*******************************************************************************
 * Copyright (c) 2012 Bryan Hunt.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Bryan Hunt - initial API and implementation
 *******************************************************************************/

package org.eclipselabs.emf.mongo.builders;

import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.xmi.XMLResource.URIHandler;
import org.eclipselabs.emf.mongo.DocumentBuilder;
import org.eclipselabs.emf.mongo.DocumentBuilderFactory;
import org.eclipselabs.emf.mongo.EObjectBuilder;
import org.eclipselabs.emf.mongo.EObjectBuilderFactory;
import org.eclipselabs.emf.mongo.converter.ConverterService;

/**
 * Default implementation of the builder factories
 * @author Mark Hoffmann
 * @since 01.07.2016
 */
@Deprecated
public class DefaultBuilderFactory implements EObjectBuilderFactory, DocumentBuilderFactory {
	/* 
	 * (non-Javadoc)
	 * @see org.eclipselabs.emf.mongo.DocumentBuilderFactory#createBuilder(org.eclipselabs.emf.mongo.converter.ConverterService, org.eclipse.emf.ecore.xmi.XMLResource.URIHandler, boolean)
	 */
	@Override
	public DocumentBuilder createBuilder(ConverterService converterService, URIHandler uriHandler, boolean serializeDefaultAttributeValues) {
		return new DocumentBuilderImpl(converterService, uriHandler, serializeDefaultAttributeValues);
	}

	/* 
	 * (non-Javadoc)
	 * @see org.eclipselabs.emf.mongo.EObjectBuilderFactory#createObjectBuilder(org.eclipselabs.emf.mongo.converter.ConverterService, org.eclipse.emf.ecore.xmi.XMLResource.URIHandler, boolean, java.util.Map)
	 */
	@Override
	public EObjectBuilder createObjectBuilder(ConverterService converterService, URIHandler uriHandler, boolean includeAttributesForProxyReferences, Map<String, EClass> eClassCache) {
		return new EObjectBuilderImpl(converterService, uriHandler, includeAttributesForProxyReferences, eClassCache);
	}
}
