/*******************************************************************************
 * Copyright (c) 2011 Bryan Hunt & Ed Merks.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Bryan Hunt & Ed Merks - initial API and implementation
 *******************************************************************************/

package org.eclipselabs.emf.mongo.streams;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipselabs.emf.mongo.EObjectBuilderFactory;
import org.eclipselabs.emf.mongo.Keywords;
import org.eclipselabs.emf.mongo.MongoUtils;
import org.eclipselabs.emf.mongo.Options;
import org.eclipselabs.emf.mongo.QueryEngine;
import org.eclipselabs.emf.mongo.codecs.EObjectCodecProvider;
import org.eclipselabs.emf.mongo.converter.ConverterService;
import org.eclipselabs.emf.mongo.converter.DefaultConverterService;
import org.eclipselabs.emf.mongo.model.EMongoCursor;
import org.eclipselabs.emf.mongo.model.EMongoQuery;
import org.eclipselabs.emf.mongo.model.ModelFactory;
import org.eclipselabs.emfosgi.EMFOSGiFactory;
import org.eclipselabs.emfosgi.EReferenceCollection;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

/**
 * @author bhunt
 * 
 */
public class MongoInputStream extends InputStream implements URIConverter.Loadable {
	
  private URI uri;
  private Map<?, ?> options;
  private QueryEngine queryEngine;
  private MongoCollection<Document> collection;
  
	public MongoInputStream(ConverterService converterService, EObjectBuilderFactory builderFactory, QueryEngine queryEngine, MongoCollection<Document> collection, URI uri, Map<?, ?> options, Map<Object, Object> response) throws IOException {
		if (converterService == null)
			throw new NullPointerException("The converter service must not be null");

		if (collection == null)
			throw new NullPointerException("The database collection must not be null");

		this.queryEngine = queryEngine;
		this.collection = collection;
		this.uri = uri;
		this.options = options;
	}

	/* 
	 * (non-Javadoc)
	 * @see org.eclipse.emf.ecore.resource.URIConverter.Loadable#loadResource(org.eclipse.emf.ecore.resource.Resource)
	 */
	@Override
	public void loadResource(Resource resource) throws IOException {
		
	  boolean createCursor = Boolean.TRUE.equals(options.get(Options.OPTION_QUERY_CURSOR));
	  
		// We need to set up the XMLResource.URIHandler so that proxy URIs are handled properly.EObjectCodecProvider codecProvider = new EObjectCodecProvider(resourceSet);
		List<Resource> resourcesCache = null;
		if(!createCursor){
		  resourcesCache = new ArrayList<Resource>(resource.getContents().size());
		}
		EObjectCodecProvider codecProvider = new EObjectCodecProvider(resource, options, resourcesCache);
		codecProvider.setConverterService(new DefaultConverterService());
		CodecRegistry eobjectRegistry = CodecRegistries.fromProviders(codecProvider);
		CodecRegistry defaultRegistry = MongoClient.getDefaultCodecRegistry();
		CodecRegistry codecRegistry = CodecRegistries.fromRegistries(eobjectRegistry, defaultRegistry);
		// get collections and clear it
		MongoCollection<EObject> curCollection = collection.withCodecRegistry(codecRegistry).withDocumentClass(EObject.class);


		// If the URI contains a query string, use it to locate a collection of objects from
		// MongoDB, otherwise simply get the object from MongoDB using the id.

		EList<EObject> contents = resource.getContents();

		if (uri.query() != null) {
			if (queryEngine == null) {
				throw new IOException("The query engine was not found");
			}

			EMongoQuery mongoQuery = queryEngine.buildMongoQuery(uri);
			FindIterable<EObject> resultIterable = null;

			if (options.containsKey(Options.OPTION_BATCH_SIZE)) {
				Object size = options.get(Options.OPTION_BATCH_SIZE);
				if (size != null && size instanceof Integer) {
					mongoQuery.setBatchSize((Integer)size);
				}
			}
			
			Document filter = mongoQuery.getFilter();
			Document projection = mongoQuery.getProjection();
			
			if (projection == null && Boolean.TRUE.equals(options.get(Options.OPTION_LAZY_RESULT_LOADING))) {
				projection = createQueryProjectionForLazyLoading();
			} 
			
			if (filter != null) {
				resultIterable = curCollection.find(filter);
			} else {
				resultIterable = curCollection.find();
			}
			
			if (projection != null) {
				resultIterable.projection(projection);
			}

			if (mongoQuery.getSkip() != null)
				resultIterable.skip(mongoQuery.getSkip());

			if (mongoQuery.getSort() != null)
				resultIterable = resultIterable.sort(mongoQuery.getSort());

			if (mongoQuery.getLimit() != null)
				resultIterable = resultIterable.limit(mongoQuery.getLimit());
			
			if (mongoQuery.getBatchSize() != null && mongoQuery.getBatchSize() > 0) {
				resultIterable.batchSize(mongoQuery.getBatchSize().intValue());
			}

			if (createCursor) {
				EMongoCursor cursor = ModelFactory.eINSTANCE.createEMongoCursor();
				cursor.setCollection(curCollection);
				cursor.setCursor(resultIterable.iterator());
//				cursor.setObjectBuilder(builder);
				contents.add(cursor);
			}
			else
			{
			  
				EReferenceCollection eCollection = EMFOSGiFactory.eINSTANCE.createEReferenceCollection();
				InternalEList<EObject> values = (InternalEList<EObject>) eCollection.getValues();
				try(MongoCursor<EObject> mongoCursor = resultIterable.iterator()){
  				while (mongoCursor.hasNext()){
  				  EObject dbObject = mongoCursor.next();
  					if(Boolean.TRUE.equals(options.get(Options.OPTION_LAZY_RESULT_LOADING))){
  						((InternalEObject) dbObject).eSetProxyURI(EcoreUtil.getURI(dbObject).appendQuery(null));
  						if(dbObject.eResource() != null){
  							Resource res = dbObject.eResource();
  							res.getContents().clear();
  							if(res.getResourceSet() != null){
  								res.getResourceSet().getResources().remove(res);
  							}
  						}
  					}
  					values.addUnique(dbObject);
  				}
				}

				contents.add(eCollection);
			}
			if(!Boolean.TRUE.equals(options.get(Options.OPTION_LAZY_RESULT_LOADING)) && !createCursor){
			    resource.getResourceSet().getResources().addAll(resourcesCache);
			}
		} else {
			EObject dbObject = curCollection.find(new Document(Keywords.ID_KEY, MongoUtils.getID(uri)), EObject.class).first();

			if (dbObject != null) {
//				EObject eObject = builder.buildEObject(collection, dbObject, resource, false);
//				if (eObject != null)
//					contents.add(eObject);

				contents.add(dbObject);
//				response.put(URIConverter.RESPONSE_TIME_STAMP_PROPERTY, dbObject.get(Keywords.TIME_STAMP_KEY));
			}
		}
	}

	/**
	 * @return the {@link Document} representing
	 */
	private Document createQueryProjectionForLazyLoading() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(Keywords.ID_KEY, 1);
		map.put(Keywords.ECLASS_KEY, 1);
		map.put(Keywords.EXTRINSIC_ID_KEY, 1);
		Document proxyProjection = new Document(map);;
		
		return proxyProjection;
	}

	/* 
	 * (non-Javadoc)
	 * @see java.io.InputStream#read()
	 */
	@Override
	public int read() throws IOException {
		// InputStream requires that we implement this function. It will never be called
		// since this implementation implements URIConverter.Loadable. The loadResource()
		// function will be called instead.

		return -1;
	}
}
