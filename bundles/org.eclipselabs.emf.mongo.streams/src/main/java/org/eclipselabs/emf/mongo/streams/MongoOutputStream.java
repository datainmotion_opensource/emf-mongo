/*******************************************************************************
 * Copyright (c) 2011 Bryan Hunt & Ed Merks.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Bryan Hunt & Ed Merks - initial API and implementation
 *    Data In Motion - update to benefit from MongoDB encoder
 *******************************************************************************/

package org.eclipselabs.emf.mongo.streams;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.types.ObjectId;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipselabs.emf.mongo.DocumentBuilderFactory;
import org.eclipselabs.emf.mongo.MongoUtils;
import org.eclipselabs.emf.mongo.Options;
import org.eclipselabs.emf.mongo.codecs.EObjectCodecProvider;
import org.eclipselabs.emf.mongo.converter.ConverterService;
import org.eclipselabs.emf.mongo.converter.DefaultConverterService;
import org.eclipselabs.emfosgi.ECollection;
import org.eclipselabs.emfosgi.EMFOSGiFactory;
import org.eclipselabs.emfosgi.EReferenceCollection;
import org.eclipselabs.mongo.osgi.api.MongoIdFactory;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.InsertOneModel;
import com.mongodb.client.model.ReplaceOneModel;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.model.WriteModel;

/**
 * @author bhunt
 * 
 */
public class MongoOutputStream extends ByteArrayOutputStream implements URIConverter.Saveable {
	
	private final boolean useIdAttributeAsPrimaryKey;
	private final boolean forceInsert;
	private final boolean clearResourceAfterInsert;
	private static final UpdateOptions UPDATE_OPTIONS = new UpdateOptions().upsert(true);
	
	public MongoOutputStream(ConverterService converterService, DocumentBuilderFactory builderFactory, MongoCollection<Document> collection, URI uri, Map<String, MongoIdFactory> idProviders, Map<?, ?> options, Map<Object, Object> response) {
		if (converterService == null) {
			throw new NullPointerException("The converter service must not be null");
		}

		if (collection == null) {
			throw new NullPointerException("The database collection must not be null");
		}

//		this.converterService = converterService;
		this.collection = collection;
		this.uri = uri;
		this.idFactories = idProviders;
		this.options = options;
		Boolean useIdAttributeAsPrimaryKey = (Boolean) options.get(Options.OPTION_USE_ID_ATTRIBUTE_AS_PRIMARY_KEY);
		this.useIdAttributeAsPrimaryKey = (useIdAttributeAsPrimaryKey == null || useIdAttributeAsPrimaryKey);
		this.forceInsert = Boolean.TRUE.equals(options.get(Options.OPTION_FORCE_INSERT));
		this.clearResourceAfterInsert = !options.containsKey(Options.OPTION_CLEAR_RESOURCE_AFTER_BATCH_INSERT) || Boolean.TRUE.equals(options.get(Options.OPTION_CLEAR_RESOURCE_AFTER_BATCH_INSERT));
	}

	/* 
	 * (non-Javadoc)
	 * @see java.io.ByteArrayOutputStream#close()
	 */
	@Override
	public void close() throws IOException {
		super.close();

		
		EObjectCodecProvider codecProvider = new EObjectCodecProvider(resource, options, null);
		codecProvider.setConverterService(new DefaultConverterService());
		CodecRegistry eobjectRegistry = CodecRegistries.fromProviders(codecProvider);
		CodecRegistry defaultRegistry = MongoClient.getDefaultCodecRegistry();
		CodecRegistry codecRegistry = CodecRegistries.fromRegistries(eobjectRegistry, defaultRegistry);
		// get collections and clear it
		MongoCollection<EObject> curCollection = collection.withCodecRegistry(codecRegistry).withDocumentClass(EObject.class);

		
//		builder = builderFactory.createBuilder(converterService, uriHandler, serializeDefaultAttributeValues);

		if (resource.getContents().size() > 1 || resource.getContents().get(0) instanceof ECollection) {
			saveMultipleObjects(curCollection);
		} else {
			EObject eObject = resource.getContents().get(0);
			EAttribute idAttribute = eObject.eClass().getEIDAttribute();
			String uriId = MongoUtils.getIDAsString(uri);
			if(idAttribute == null && useIdAttributeAsPrimaryKey){
				throw new IllegalStateException("EObject has no ID Attribute to be used together with option " +  Options.OPTION_USE_ID_ATTRIBUTE_AS_PRIMARY_KEY);
			} else if(!useIdAttributeAsPrimaryKey){
				if(uriId == null || uriId.isEmpty()){
					Object id = null;
					MongoIdFactory mongoIdFactory = idFactories.get(uri.trimSegments(uri.segmentCount() - 2).toString());
					if (mongoIdFactory != null) {
						id = mongoIdFactory.getNextId();
					} else {
						id = new ObjectId();
					}
					uri = uri.trimSegments(1).appendSegment(id.toString());
					resource.setURI(uri);
				}
			} else {
				Object objectId = eObject.eGet(idAttribute);
				if(objectId != null){
					if(uriId == null || uriId.isEmpty()){
						resource.setURI(resource.getURI().trimSegments(1).appendSegment(objectId.toString()));
						uri = resource.getURI();
					}
				} else {
					if(uriId != null && !uriId.isEmpty()){
						eObject.eSet(idAttribute, EcoreUtil.createFromString(idAttribute.getEAttributeType(), uriId));
					} else {
						MongoIdFactory mongoIdFactory = idFactories.get(uri.trimSegments(uri.segmentCount() - 2).toString());
						Object newId = null;
						if (mongoIdFactory != null) {
							newId = mongoIdFactory.getNextId();
						} else {
							newId = new ObjectId();
						}
						uri = uri.trimSegments(1).appendSegment(newId.toString());
						resource.setURI(uri);
						eObject.eSet(idAttribute, EcoreUtil.createFromString(idAttribute.getEAttributeType(), newId.toString()));
					}
				}
			}
			saveSingleObject(curCollection);
		}
	}
	
	/* 
	 * (non-Javadoc)
	 * @see org.eclipse.emf.ecore.resource.URIConverter.Saveable#saveResource(org.eclipse.emf.ecore.resource.Resource)
	 */
	@Override
	public void saveResource(Resource resource) {
		this.resource = resource;
	}

	private void saveMultipleObjects(MongoCollection<EObject> collection) throws IOException {
		resource.setURI(resource.getURI().trimSegments(1).appendSegment("-1"));
		List<EObject> contents = null;

		if (resource.getContents().get(0) instanceof ECollection) {
			contents = ((ECollection) resource.getContents().get(0)).getValues();
		} else {
			contents = resource.getContents();
		}
//
		
		List<WriteModel<EObject>> bulk = new ArrayList<>(contents.size()); 
		for (EObject eObject : contents) {
			EAttribute idAttribute = eObject.eClass().getEIDAttribute();

			if (idAttribute != null && useIdAttributeAsPrimaryKey) {
				Object id = eObject.eGet(idAttribute);
				if(id == null){
					MongoIdFactory mongoIdFactory = idFactories.get(uri.trimSegments(uri.segmentCount() - 2).toString());
					Object newId = null;
					if (mongoIdFactory != null) {
						newId = mongoIdFactory.getNextId();
					} else {
						newId = new ObjectId();
					}
					eObject.eSet(idAttribute, EcoreUtil.createFromString(idAttribute.getEAttributeType(), newId.toString()));
					id = newId;
				}
				if(forceInsert){
					bulk.add(new InsertOneModel<EObject>(eObject));
				} else {
					if(ObjectId.isValid(id.toString())){
						id = new ObjectId(id.toString());
					}
					bulk.add(new ReplaceOneModel<EObject>(Filters.eq("_id", id), eObject, UPDATE_OPTIONS));
				}
			} else if(idAttribute == null && useIdAttributeAsPrimaryKey){
				throw new IllegalStateException("EObjects have no ID Attribute to be used together with option " +  Options.OPTION_USE_ID_ATTRIBUTE_AS_PRIMARY_KEY);
			}
		}
		if(!useIdAttributeAsPrimaryKey){
			collection.insertMany(contents);
		} else {
			collection.bulkWrite(bulk);
		}

		if(clearResourceAfterInsert){
			resource.getContents().clear();
		} else {
			URI baseURI = resource.getURI().trimSegments(1);
			InternalEObject[] eObjects = contents.toArray(new InternalEObject[contents.size()]);
			EReferenceCollection eCollection = EMFOSGiFactory.eINSTANCE.createEReferenceCollection();
			InternalEList<EObject> values = (InternalEList<EObject>) eCollection.getValues();
			
			for (int i = 0; i < contents.size(); i++) {
				InternalEObject internalEObject = eObjects[i];
				internalEObject.eSetProxyURI(baseURI.appendSegment(EcoreUtil.getID(internalEObject)).appendFragment("/"));
				internalEObject.eAdapters().clear();
				values.addUnique(internalEObject);
			}
			resource.getContents().clear();
			resource.getContents().add(eCollection);
		}
	}

	private void saveSingleObject(MongoCollection<EObject> collection) throws IOException {
		EObject eObject = resource.getContents().get(0);
		if(forceInsert){
			collection.insertOne(eObject);
		} else {
			Object id = EcoreUtil.getID(eObject);
			if(id == null){
				id = MongoUtils.getID(uri);
			} else {
				if(ObjectId.isValid(id.toString())){
					id = new ObjectId(id.toString());
				} else {
					id = id.toString();
				}
			}
			
			collection.replaceOne(Filters.eq("_id", id), eObject, UPDATE_OPTIONS);
		}
	}

//	private ConverterService converterService;
//	private DocumentBuilderFactory builderFactory;
	private MongoCollection<Document> collection;
	private Map<?, ?> options;
	private Resource resource;
	private URI uri;
	private Map<String, MongoIdFactory> idFactories;
}
