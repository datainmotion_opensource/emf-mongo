/*******************************************************************************
 * Copyright (c) 2012 Bryan Hunt.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Bryan Hunt - initial API and implementation
 *******************************************************************************/

package org.eclipselabs.emf.mongo.streams;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.bson.Document;
import org.eclipse.emf.common.util.URI;
import org.eclipselabs.emf.mongo.DocumentBuilderFactory;
import org.eclipselabs.emf.mongo.EObjectBuilderFactory;
import org.eclipselabs.emf.mongo.InputStreamFactory;
import org.eclipselabs.emf.mongo.OutputStreamFactory;
import org.eclipselabs.emf.mongo.QueryEngine;
import org.eclipselabs.emf.mongo.converter.ConverterService;
import org.eclipselabs.mongo.osgi.api.MongoIdFactory;

import com.mongodb.client.MongoCollection;

/**
 * @author bhunt
 * 
 */
public class DefaultStreamFactory implements InputStreamFactory, OutputStreamFactory {
	
	private DocumentBuilderFactory dbObjectBuilderFactory;
	private EObjectBuilderFactory eObjectBuilderFactory;
	private QueryEngine queryEngine;
	private ConverterService converterService;
	private volatile Map<String, MongoIdFactory> idFactories = new ConcurrentHashMap<>();
	/* 
	 * (non-Javadoc)
	 * @see org.eclipselabs.emf.mongo.OutputStreamFactory#createOutputStream(org.eclipse.emf.common.util.URI, java.util.Map, com.mongodb.client.MongoCollection, java.util.Map)
	 */
	@Override
	public OutputStream createOutputStream(URI uri, Map<?, ?> options, MongoCollection<Document> collection, Map<Object, Object> response) {
		return new MongoOutputStream(converterService, dbObjectBuilderFactory, collection, uri, idFactories, options, response);
	}

	/* (non-Javadoc)
	 * @see org.eclipselabs.emf.mongo.InputStreamFactory#createInputStream(org.eclipse.emf.common.util.URI, java.util.Map, com.mongodb.client.MongoCollection, java.util.Map)
	 */
	@Override
	public InputStream createInputStream(URI uri, Map<?, ?> options, MongoCollection<Document> collection, Map<Object, Object> response) throws IOException {
		return new MongoInputStream(converterService, eObjectBuilderFactory, queryEngine, collection, uri, options, response);
	}

	public void bindConverterService(ConverterService converterService) {
		this.converterService = converterService;
	}

	public void bindDocumentBuilderFactory(DocumentBuilderFactory dbObjectBuilderFactory) {
		this.dbObjectBuilderFactory = dbObjectBuilderFactory;
	}

	public void bindEObjectBuilderFactory(EObjectBuilderFactory eObjectBuilderFactory) {
		this.eObjectBuilderFactory = eObjectBuilderFactory;
	}

	public void bindQueryEngine(QueryEngine queryEngine) {
		this.queryEngine = queryEngine;
	}

	public synchronized void bindMongoIdFactory(MongoIdFactory mongoIdFactory) {
		idFactories.put(mongoIdFactory.getCollectionURI(), mongoIdFactory);
	}

	public void unbindMongoIdFactory(MongoIdFactory mongoIdFactory) {
		MongoIdFactory target = idFactories.get(mongoIdFactory.getCollectionURI());

		if (mongoIdFactory == target)
			idFactories.remove(mongoIdFactory.getCollectionURI());
	}
}
