/*******************************************************************************
 * Copyright (c) 2012 Bryan Hunt.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Bryan Hunt - initial API and implementation
 *******************************************************************************/

package org.eclipselabs.emf.mongo;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.ecore.resource.impl.BinaryResourceImpl;

/**
 * @author bhunt
 * 
 */
public interface Options {
	
	/**
	 * This option can be set to append the given value from the map to the default collection name.
	 * The MongoURIHandler uses this option
	 */
	String OPTIONS_COLLECTION_PARTITION_EXTENSION = "COLLECTION_PARTITION_EXTENSION";

	/**
	 * When you load a resource with a query, only information that are necessary to build a proxy 
	 * will be loaded from the db. Thus the results will be lazy while iterating over the returned 
	 * {@link ECollections}.
	 * 
	 * This only works with query without projection.
	 * 
	 * value type: Boolean
	 */
	String OPTION_LAZY_RESULT_LOADING = "LAZY_RESULT_LOADING";
	
	/**
	 * When you load an object with cross-document references, they will be proxies. When you access
	 * the reference, EMF will resolve the proxy and you can then access the attributes. This can
	 * cause
	 * performance problems for example when expanding a tree where you only need a name attribute to
	 * display the children and then only resolve the next child to be expanded. Setting this option
	 * to
	 * Boolean.TRUE will cause the proxy instance to have its attribute values populated so that you
	 * can display the child names in the tree without resolving the proxy.
	 * 
	 * Value type: Boolean
	 */
	String OPTION_PROXY_ATTRIBUTES = BinaryResourceImpl.OPTION_STYLE_PROXY_ATTRIBUTES;

	/**
	 * EMF's default serialization is designed to conserve space by not serializing attributes that
	 * are set to their default value. This is a problem when attempting to query objects by an
	 * attributes
	 * default value. By setting this option to Boolean.TRUE, all attribute values will be stored to
	 * MongoDB.
	 * 
	 * Value type: Boolean
	 */
	String OPTION_SERIALIZE_DEFAULT_ATTRIBUTE_VALUES = "SERIALIZE_DEFAULT_ATTRIBUTE_VALUES";

	/**
	 * To avoid writing unnecessary URIs in the DB, mongo emf writes eClassUris only for the root class and for 
	 * EReferences, where the actual value does not equal but inherit from the stated reference type. 
	 * default value. By setting this option to Boolean.TRUE, all eClass URIs will be written regardless. 
	 * 
	 * Value type: Boolean
	 */
	String OPTION_SERIALIZE_ALL_ECLASS_URIS = "SERIALIZE_ALL_ECLASS_URIS";

	/**
	 * If it is set to Boolean.TRUE and the ID was not specified in the URI, the value of the ID
	 * attribute will be used as the MongoDB _id if it exists.
	 * 
	 * Value type: Boolean
	 */
	String OPTION_USE_ID_ATTRIBUTE_AS_PRIMARY_KEY = "USE_ID_ATTRIBUTE_AS_PRIMARY_KEY";

	/**
	 * If set, the value must be an instance of WriteConcern and will be passed to MongoDB when the
	 * object is inserted into the database, or updated.
	 * 
	 * Value type: WriteConcern
	 */
	String OPTION_WRITE_CONCERN = "WRITE_CONCERN";

	/**
	 * If set to Boolean.TRUE, a query will return a EMongoCursor instead of a Result
	 * 
	 * Value type: Boolean
	 */
	String OPTION_QUERY_CURSOR = "QUERY_CURSOR";

	/**
	 * This option may be used when you wish to read from a particular server in a MongoDB
	 * replica set that has been tagged.
	 * 
	 * <code>
	 * ReadPreference readPreference = ReadPreference.primaryPreferred(new BasicDbObject("datacenter", "Austin"));
	 * resourceSet.getLoadOptions().put(Options.OPTION_READ_PREFERENCE, readPreference);
	 * </code>
	 * 
	 * Value type: ReadPreference
	 */
	String OPTION_READ_PREFERENCE = "READ_PREFERENCE";
	
	/**
	 * This option may be used when you wish to set a non standard batch size for writing data or reading data from a MongoDB
	 * 
	 * <code>
	 * resourceSet.getLoadOptions().put(Options.OPTION_BATCH_SIZE, 400);
	 * </code>
	 * 
	 * Value type: {@link Integer}
	 */
	String OPTION_BATCH_SIZE = "BATCH_SIZE";

	/**
	 * This option may be used when you wish to force an insert even if there is an ID set. Default is false, which would urge 
	 * mongoDB to perform an update.
	 * 
	 * <code>
	 * resourceSet.getLoadOptions().put(Options.OPTION_FORCE_INSERT, Boolean.TRUE);
	 * </code>
	 * 
	 * Value type: {@link Boolean}
	 */
	String OPTION_FORCE_INSERT = "FORCE_INSERT";

	/**
	 * If set to true the resource containing the batch will be cleared after saving and will not be populated with proxies
	 * 
	 * <code>
	 * resourceSet.getLoadOptions().put(Options.OPTION_CLEAR_RESOURCE_AFTER_BATCH_INSERT, Boolean.TRUE);
	 * </code>
	 * 
	 * Value type: {@link Boolean}
	 */
	String OPTION_CLEAR_RESOURCE_AFTER_BATCH_INSERT = "CLEAR_RESOURCE_AFTER_BATCH_INSERT";
}
