/*******************************************************************************
 * Copyright (c) 2012 Bryan Hunt.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Bryan Hunt - initial API and implementation
 *******************************************************************************/

package org.eclipselabs.emf.mongo;

import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipselabs.emf.mongo.converter.ConverterService;

/**
 * This interface provides the API for for the DocumentBuilderFactory factory. If you wish to use a custom {@link DocumentBuilderFactory},
 * you must create a factory class for your builder that implements this interface.
 * 
 * @author Mark Hoffmann
 * @since 01.07.2016
 */
public interface DocumentBuilderFactory
{
	/**
	 * Constructs a DBObjectBuilder
	 * 
	 * @param converterService the converter service to use for converting non-native values
	 * @param uriHandler the uri handler to use for creating relative URIs
	 * @param serializeDefaultAttributeValues true indicates that default attribute values must be stored in the DBObject; false otherwise
	 * @return the {@link DocumentBuilder} builder
	 */
	DocumentBuilder createBuilder(ConverterService converterService, XMLResource.URIHandler uriHandler, boolean serializeDefaultAttributeValues);
}
